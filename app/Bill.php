<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $fillable = [
        'bill_no', 'bill_date', 'due_date',
        'title', 'sub_total', 'discount',
        'grand_total', 'client',
        'client_address'
    ];

    public function products()
    {
        return $this->hasMany(BillProduct::class);
    }
}
