<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'sales_no', 'sales_date', 'due_date',
        'title', 'sub_total', 'discount',
        'grand_total', 'sales_tnc', 'sales_note', 'sales_delivery_method', 'sales_person', 'sales_reference', 'sales_order', 'sales_customer_name',
        'client_address'
    ];

    public function products()
    {
        return $this->hasMany(SaleProduct::class);
    }
}
