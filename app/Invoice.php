<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'invoice_no', 'invoice_date', 'due_date', 'order_number', 'sales_person', 'sub_total', 'discount', 'tnc', 'notes', 'terms',
        'grand_total', 'client',
        'client_address'
    ];

    public function products()
    {
        return $this->hasMany(InvoiceProduct::class);
    }
}
