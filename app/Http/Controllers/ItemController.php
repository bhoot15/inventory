<?php

namespace App\Http\Controllers;

use App\Item;
use Auth;
use DB;
use Illuminate\Http\Request;

class ItemController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = DB::table('items')->get();
        $units = DB::table('uoms')->select('item_attribute')->get();
        $suppliers = DB::table('suppliers')->select('supplier_name')->get();
        return view('item.items', compact('items', 'units', 'suppliers'));
    }
    public function create()
    {
        $count = DB::table('items')->count();
        return view('item.create',compact('count'));
    }
    public function store(Request $request)
    {
       Item::create($request->all());  //method 1 to return full table
        return redirect('/item');
    }

    public function show($id)
    {
       // $item = item::findOrFail($id);
        return view('item.show');
    }

    public function edit($id)
    {
        $units = DB::table('uoms')->select('item_attribute')->get();
        $suppliers = DB::table('suppliers')->select('supplier_name')->get();
        $item = Item::findOrFail($id);
        return view('item.edit', compact('item', 'units', 'suppliers'));
    }
    public function update(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        $input = $request->all();
        $item->fill($input)->save();
        return redirect('/item');

    }
    public function destroy($id){
        Item::destroy($id);
        return redirect('/item');
        /*$ids = $request->input('checkboxes');
        item::destroy($ids);*/

    }
}
