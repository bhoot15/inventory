<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Sale;
use App\SaleProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesController extends Controller
{
    public function index()
    {
        $sales = Sale::orderBy('created_at', 'desc')
            ->paginate(8);

        return view('sales.index', compact('sales'));
    }

    public function create()
    {
        $suppliers = DB::table('suppliers')
            ->select('supplier_name')
            ->where('deleted_at', '=', null)
            ->get();
        $customers = DB::table('customers')
            ->select('customer_name')
            ->where('deleted_at', '=', null)
            ->get();
        $items = DB::table('items')
            ->select('item_name', 'item_sales_selling_price', 'item_purchase_selling_price', 'item_sales_tax')
            ->where('deleted_at', '=', null)
            ->get();
        return view('sales.create', compact('suppliers', 'customers', 'items'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'sales_no' => 'required|alpha_dash|unique:sales',
            'sales_delivery_method' => 'required|max:255',
            'sales_person' => 'required|max:255',
            'sales_reference' => 'required|max:255',
            'sales_order' => 'required|max:255',
            'sales_customer_name' => 'required|max:255',
            'client_address' => 'required|max:255',
            'sales_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'title' => 'required|max:255',
            'discount' => 'required|numeric|min:0',
            'products.*.name' => 'required|max:255',
            'products.*.price' => 'required|numeric|min:1',
            'products.*.qty' => 'required|integer|min:1'
        ]);

        $products = collect($request->products)->transform(function ($product) {
            $product['total'] = $product['qty'] * $product['price'];
            return new SaleProduct($product);
        });

        if ($products->isEmpty()) {
            return response()
                ->json([
                    'products_empty' => ['One or more Product is required.']
                ], 422);
        }

        $data = $request->except('products');
        $data['sub_total'] = $products->sum('total');
        $data['grand_total'] = $data['sub_total'] - $data['discount'];

        $sale = Sale::create($data);

        $sale->products()->saveMany($products);

        return response()
            ->json([
                'created' => true,
                'id' => $sale->id
            ]);
    }

    public function show($id)
    {
        $sale = Sale::with('products')->findOrFail($id);
        return view('sales.show', compact('sale'));
    }

    public function edit($id)
    {
        $suppliers = DB::table('suppliers')
            ->select('supplier_name')
            ->where('deleted_at', '=', null)
            ->get();
        $customers = DB::table('customers')
            ->select('customer_name')
            ->where('deleted_at', '=', null)
            ->get();
        $items = DB::table('items')
            ->select('item_name', 'item_sales_selling_price', 'item_purchase_selling_price', 'item_sales_tax')
            ->where('deleted_at', '=', null)
            ->get();
        $sale = Sale::with('products')->findOrFail($id);
        return view('sales.edit', compact('sale', 'suppliers', 'customers', 'items'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'sales_no' => 'required|alpha_dash|unique:sales,sales_no,' . $id . ',id',
            'sales_delivery_method' => 'required|max:255',
            'sales_person' => 'required|max:255',
            'sales_reference' => 'required|max:255',
            'sales_order' => 'required|max:255',
            'sales_customer_name' => 'required|max:255',
            'client_address' => 'required|max:255',
            'sales_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'title' => 'required|max:255',
            'discount' => 'required|numeric|min:0',
            'products.*.name' => 'required|max:255',
            'products.*.price' => 'required|numeric|min:1',
            'products.*.qty' => 'required|integer|min:1'
        ]);

        $sale = Sale::findOrFail($id);

        $products = collect($request->products)->transform(function ($product) {
            $product['total'] = $product['qty'] * $product['price'];
            return new SaleProduct($product);
        });

        if ($products->isEmpty()) {
            return response()
                ->json([
                    'products_empty' => ['One or more Product is required.']
                ], 422);
        }

        $data = $request->except('products');
        $data['sub_total'] = $products->sum('total');
        $data['grand_total'] = $data['sub_total'] - $data['discount'];

        $sale->update($data);

        SaleProduct::where('sale_id', $sale->id)->delete();

        $sale->products()->saveMany($products);

        return response()
            ->json([
                'updated' => true,
                'id' => $sale->id
            ]);
    }

    public function destroy($id)
    {
        $sale = Sale::findOrFail($id);

        SaleProduct::where('sale_id', $sale->id)
            ->delete();

        $sale->delete();

        return redirect('/sales');
    }

    public function salesOutstanding()
    {
        return view('sales-outstanding.sales-outstandings');
    }
}
