<?php

namespace App\Http\Controllers;

use App\Customer;
use Auth;
use DB;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = DB::table('customers')->get();
        return view('customers.customer', compact('customers'));
    }
    public function create()
    {
       $supplier = DB::table('suppliers')
            ->select('id')->get();

        $customer = DB::table('customers')
            ->get();
        return view('customers.create', compact('supplier','customer'));
    }
    public function store(Request $request)
    {
        Customer::create($request->all());  //method 1 to return full table
        //return redirect('/customer');
        return redirect()->back();
    }

    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        return view('customers.show',compact('customer'));
        /*$purchase = Purchase::with('products')->findOrFail($id);
        return view('purchases.edit', compact('purchase'));*/
    }

    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        return view('customers.edit',compact('customer'));
    }
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $input = $request->all();
        $customer->fill($input)->save();
        return redirect('/customer');

    }
    public function destroy($id){
        Customer::destroy($id);
        return redirect('/customer');
    }
}
