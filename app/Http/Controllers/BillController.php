<?php

namespace App\Http\Controllers;

use App\Bill;
use App\BillProduct;
use App\Http\Requests;
use Illuminate\Http\Request;

class BillController extends Controller
{
    public function index()
    {
        $bills = Bill::orderBy('created_at', 'desc')
            ->paginate(8);

        return view('bills.index', compact('bills'));
    }

    public function create()
    {
        return view('bills.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'bill_no' => 'required|alpha_dash|unique:bills',
            'client' => 'required|max:255',
            'client_address' => 'required|max:255',
            'bill_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'title' => 'required|max:255',
            'discount' => 'required|numeric|min:0',
            'products.*.name' => 'required|max:255',
            'products.*.price' => 'required|numeric|min:1',
            'products.*.qty' => 'required|integer|min:1'
        ]);

        $products = collect($request->products)->transform(function ($product) {
            $product['total'] = $product['qty'] * $product['price'];
            return new BillProduct($product);
        });

        if ($products->isEmpty()) {
            return response()
                ->json([
                    'products_empty' => ['One or more Product is required.']
                ], 422);
        }

        $data = $request->except('products');
        $data['sub_total'] = $products->sum('total');
        $data['grand_total'] = $data['sub_total'] - $data['discount'];

        $bill = Bill::create($data);

        $bill->products()->saveMany($products);

        return response()
            ->json([
                'created' => true,
                'id' => $bill->id
            ]);
    }

    public function show($id)
    {
        $bill = Bill::with('products')->findOrFail($id);
        return view('bills.show', compact('bill'));
    }

    public function edit($id)
    {
        $bill = Bill::with('products')->findOrFail($id);
        return view('bills.edit', compact('bill'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bill_no' => 'required|alpha_dash|unique:bills,bill_no,' . $id . ',id',
            'client' => 'required|max:255',
            'client_address' => 'required|max:255',
            'bill_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'title' => 'required|max:255',
            'discount' => 'required|numeric|min:0',
            'products.*.name' => 'required|max:255',
            'products.*.price' => 'required|numeric|min:1',
            'products.*.qty' => 'required|integer|min:1'
        ]);

        $bill = Bill::findOrFail($id);

        $products = collect($request->products)->transform(function ($product) {
            $product['total'] = $product['qty'] * $product['price'];
            return new BillProduct($product);
        });

        if ($products->isEmpty()) {
            return response()
                ->json([
                    'products_empty' => ['One or more Product is required.']
                ], 422);
        }

        $data = $request->except('products');
        $data['sub_total'] = $products->sum('total');
        $data['grand_total'] = $data['sub_total'] - $data['discount'];

        $bill->update($data);

        BillProduct::where('bill_id', $bill->id)->delete();

        $bill->products()->saveMany($products);

        return response()
            ->json([
                'updated' => true,
                'id' => $bill->id
            ]);
    }

    public function destroy($id)
    {
        $bill = Bill::findOrFail($id);

        BillProduct::where('bill_id', $bill->id)
            ->delete();

        $bill->delete();

        return redirect()
            ->route('bills.index');
    }

    public function billsOutstanding()
    {
        return view('bills-outstanding.bills-outstandings');
    }
}
