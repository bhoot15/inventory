<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Invoice;
use App\InvoiceProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public function index()
    {
        $invoices = Invoice::orderBy('created_at', 'desc')
            ->paginate(8);

        return view('invoices.index', compact('invoices'));
    }

    public function create()
    {
        $suppliers = DB::table('suppliers')
            ->select('supplier_name')
            ->where('deleted_at', '=', null)
            ->get();
        $customers = DB::table('customers')
            ->select('customer_name')
            ->where('deleted_at', '=', null)
            ->get();

        $items = DB::table('items')
            ->select('item_name', 'item_sales_selling_price', 'item_purchase_selling_price', 'item_sales_tax')
            ->where('deleted_at', '=', null)
            ->get();
        return view('invoices.create', compact('items', 'suppliers', 'customers'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'invoice_no' => 'required|alpha_dash|unique:invoices',
            'client' => 'required|max:255',
            'client_address' => 'required|max:255',
            'invoice_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'discount' => 'required|numeric|min:0',
            'products.*.name' => 'required|max:255',
            'products.*.price' => 'required|numeric|min:1',
            'products.*.qty' => 'required|integer|min:1'
        ]);

        $products = collect($request->products)->transform(function($product) {
            $product['total'] = $product['qty'] * $product['price'];
            return new InvoiceProduct($product);
        });

        if($products->isEmpty()) {
            return response()
            ->json([
                'products_empty' => ['One or more Product is required.']
            ], 422);
        }

        $data = $request->except('products');
        $data['sub_total'] = $products->sum('total');
        $data['grand_total'] = $data['sub_total'] - $data['discount'];

        $invoice = Invoice::create($data);

        $invoice->products()->saveMany($products);

        return response()
            ->json([
                'created' => true,
                'id' => $invoice->id
            ]);
    }

    public function show($id)
    {
        $invoice = Invoice::with('products')->findOrFail($id);

        $supplier = DB::table('suppliers')
            ->join('invoices', 'suppliers.supplier_name', '=', 'invoices.sales_person')
            ->where('invoices.id', '=', $id)
            ->get();

        $customer = DB::table('customers')
            ->join('invoices', 'customers.customer_name', '=', 'invoices.client')
            ->where('invoices.id', '=', $id)
            ->get();

        return view('invoices.show', compact('invoice', 'supplier', 'customer'));
    }

    public function edit($id)
    {
        $suppliers = DB::table('suppliers')
            ->select('supplier_name')
            ->where('deleted_at', '=', null)
            ->get();
        $customers = DB::table('customers')
            ->select('customer_name')
            ->where('deleted_at', '=', null)
            ->get();

        $items = DB::table('items')
            ->select('item_name', 'item_sales_selling_price', 'item_purchase_selling_price', 'item_sales_tax')
            ->get();

        $invoice = Invoice::with('products')->findOrFail($id);

        return view('invoices.edit', compact('invoice', 'items', 'suppliers', 'customers'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'invoice_no' => 'required|alpha_dash|unique:invoices,invoice_no,'.$id.',id',
            'client' => 'required|max:255',
            'client_address' => 'required|max:255',
            'invoice_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'discount' => 'required|numeric|min:0',
            'products.*.name' => 'required|max:255',
            'products.*.price' => 'required|numeric|min:1',
            'products.*.qty' => 'required|integer|min:1'
        ]);

        $invoice = Invoice::findOrFail($id);

        $products = collect($request->products)->transform(function($product) {
            $product['total'] = $product['qty'] * $product['price'];
            return new InvoiceProduct($product);
        });

        if($products->isEmpty()) {
            return response()
            ->json([
                'products_empty' => ['One or more Product is required.']
            ], 422);
        }

        $data = $request->except('products');
        $data['sub_total'] = $products->sum('total');
        $data['grand_total'] = $data['sub_total'] - $data['discount'];

        $invoice->update($data);

        InvoiceProduct::where('invoice_id', $invoice->id)->delete();

        $invoice->products()->saveMany($products);

        return response()
            ->json([
                'updated' => true,
                'id' => $invoice->id
            ]);
    }

    public function destroy($id)
    {
        $invoice = Invoice::findOrFail($id);

        InvoiceProduct::where('invoice_id', $invoice->id)
            ->delete();

        $invoice->delete();

        return redirect()
            ->route('invoices.index');
    }

    public function invoicePrint($id)
    {
        $invoice = Invoice::with('products')->findOrFail($id);

        $supplier = DB::table('suppliers')
            ->join('invoices', 'suppliers.supplier_name', '=', 'invoices.sales_person')
            ->where('invoices.id', '=', $id)
            ->get();

        $customer = DB::table('customers')
            ->join('invoices', 'customers.customer_name', '=', 'invoices.client')
            ->where('invoices.id', '=', $id)
            ->get();

        return view('invoices.print', compact('invoice', 'supplier', 'customer'));

    }
}
