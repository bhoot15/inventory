<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;
use DB;
use Auth;

class SupplierController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = DB::table('suppliers')->get();
        return view('suppliers.supplier',compact('suppliers'));
    }
    public function create()
    {
        return view('suppliers.create');
    }
    public function store(Request $request)
    {
        Supplier::create($request->all());  //method 1 to return full table
        return redirect('/supplier');
    }

    public function show($id)
    {
        $supplier = Supplier::findOrFail($id);
        return view('suppliers.show',compact('supplier'));
    }

    public function edit($id)
    {
        $supplier = Supplier::findOrFail($id);
        return view('suppliers.edit',compact('supplier'));
    }
    public function update(Request $request, $id)
    {
        $supplier = Supplier::findOrFail($id);
        $input = $request->all();
        $supplier->fill($input)->save();
        return redirect('/supplier');

    }
    public function destroy($id){
        Supplier::destroy($id);
        return redirect('/supplier');
    }
}
