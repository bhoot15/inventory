<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Purchase;
use App\PurchaseProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    public function index()
    {
        $purchases = Purchase::orderBy('created_at', 'desc')
            ->paginate(8);

        return view('purchases.index', compact('purchases'));
    }

    public function create()
    {
        $suppliers = DB::table('suppliers')
            ->select('supplier_name')
            ->where('deleted_at', '=', null)
            ->get();
        $customers = DB::table('customers')
            ->select('customer_name')
            ->where('deleted_at', '=', null)
            ->get();
        $items = DB::table('items')
            ->select('item_name', 'item_sales_selling_price', 'item_purchase_selling_price', 'item_sales_tax')
            ->where('deleted_at', '=', null)
            ->get();
        return view('purchases.create', compact('suppliers', 'customers', 'items'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'purchase_no' => 'required|alpha_dash|unique:purchases',
            'supplier_name' => 'required|max:255',
            'purchase_reference' => 'required|max:255',
            'purchase_order_number' => 'required|max:255',
            'client' => 'required|max:255',
            'client_address' => 'required|max:255',
            'purchase_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'title' => 'required|max:255',
            'discount' => 'required|numeric|min:0',
            'products.*.name' => 'required|max:255',
            'products.*.price' => 'required|numeric|min:1',
            'products.*.qty' => 'required|integer|min:1'
        ]);

        $products = collect($request->products)->transform(function ($product) {
            $product['total'] = $product['qty'] * $product['price'];
            return new PurchaseProduct($product);
        });

        if ($products->isEmpty()) {
            return response()
                ->json([
                    'products_empty' => ['One or more Product is required.']
                ], 422);
        }

        $data = $request->except('products');
        $data['sub_total'] = $products->sum('total');
        $data['grand_total'] = $data['sub_total'] - $data['discount'];

        $purchase = Purchase::create($data);

        $purchase->products()->saveMany($products);

        return response()
            ->json([
                'created' => true,
                'id' => $purchase->id
            ]);
    }

    public function show($id)
    {
        $purchase = Purchase::with('products')->findOrFail($id);

        $supplier = DB::table('suppliers')
            ->join('purchases', 'suppliers.supplier_name', '=', 'purchases.supplier_name')
            ->where('purchases.id', '=', $id)
            ->get();

        $customer = DB::table('customers')
            ->join('purchases', 'customers.customer_name', '=', 'purchases.client')
            ->where('purchases.id', '=', $id)
            ->get();

        return view('purchases.show', compact('purchase', 'supplier', 'customer'));
        //return dd($supplier);
    }

    public function edit($id)
    {
        $purchase = Purchase::with('products')->findOrFail($id);

        $suppliers = DB::table('suppliers')
            ->select('supplier_name')
            ->get();
        $customers = DB::table('customers')
            ->select('customer_name')
            ->get();
        $items = DB::table('items')
            ->select('item_name', 'item_sales_selling_price', 'item_purchase_selling_price', 'item_sales_tax')
            ->where('deleted_at', '=', null)
            ->get();

        return view('purchases.edit', compact('purchase', 'suppliers', 'customers', 'items'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'purchase_no' => 'required|alpha_dash|unique:purchases,purchase_no,' . $id . ',id',
            'supplier_name' => 'required|max:255',
            'purchase_reference' => 'required|max:255',
            'purchase_order_number' => 'required|max:255',
            'client' => 'required|max:255',
            'client_address' => 'required|max:255',
            'purchase_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'title' => 'required|max:255',
            'discount' => 'required|numeric|min:0',
            'products.*.name' => 'required|max:255',
            'products.*.price' => 'required|numeric|min:1',
            'products.*.qty' => 'required|integer|min:1'
        ]);

        $purchase = Purchase::findOrFail($id);

        $products = collect($request->products)->transform(function ($product) {
            $product['total'] = $product['qty'] * $product['price'];
            return new PurchaseProduct($product);
        });

        if ($products->isEmpty()) {
            return response()
                ->json([
                    'products_empty' => ['One or more Product is required.']
                ], 422);
        }

        $data = $request->except('products');
        $data['sub_total'] = $products->sum('total');
        $data['grand_total'] = $data['sub_total'] - $data['discount'];

        $purchase->update($data);

        PurchaseProduct::where('purchase_id', $purchase->id)->delete();

        $purchase->products()->saveMany($products);

        return response()
            ->json([
                'updated' => true,
                'id' => $purchase->id
            ]);
    }

    public function destroy($id)
    {
        $purchase = Purchase::findOrFail($id);

        PurchaseProduct::where('purchase_id', $purchase->id)
            ->delete();

        $purchase->delete();

        return redirect('/purchases');
    }

    public function purchaseOutstanding()
    {
        return view('purchase-outstanding.purchase-outstandings');
    }

    public function purchasePrint($id)
    {
        $purchase = Purchase::with('products')->findOrFail($id);

        $supplier = DB::table('suppliers')
            ->join('purchases', 'suppliers.supplier_name', '=', 'purchases.supplier_name')
            ->where('purchases.id', '=', $id)
            ->get();

        $customer = DB::table('customers')
            ->join('purchases', 'customers.customer_name', '=', 'purchases.client')
            ->where('purchases.id', '=', $id)
            ->get();

        return view('purchases.print', compact('purchase', 'supplier', 'customer'));

    }
}
