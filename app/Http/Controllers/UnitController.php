<?php

namespace App\Http\Controllers;

use App\Uom;
use Auth;
use DB;
use Illuminate\Http\Request;

class UnitController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = DB::table('uoms')->get();
        return view('unit.uom', compact('units'));
    }
    public function create()
    {
        return view('unit.create');
    }
    public function store(Request $request)
    {
        Uom::create($request->all());  //method 1 to return full table
        return redirect()->back();
    }

    public function show($id)
    {
       // $unit = Unit::findOrFail($id);
        return view('unit.show');
    }

    public function edit($id)
    {
        $unit = Uom::findOrFail($id);
        return view('unit.edit', compact('unit'));
    }
    public function update(Request $request, $id)
    {
        $unit = Uom::findOrFail($id);
        $input = $request->all();
        $unit->fill($input)->save();
        return redirect('/uom');

    }
    public function destroy($id){
        Uom::destroy($id);
        return redirect('/uom');
    }
}
