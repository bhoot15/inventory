<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = [
        'purchase_no', 'purchase_date', 'due_date',
        'title', 'sub_total', 'discount', 'sales_tnc', 'sales_note', 'purchase_reference', 'purchase_order_number',
        'grand_total', 'client', 'supplier_name',
        'client_address'
    ];

    public function products()
    {
        return $this->hasMany(PurchaseProduct::class);
    }
}
