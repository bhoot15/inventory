<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;
    protected $table = "items";
    protected $fillable = ['item_name', 'item_sku', 'item_unit', 'item_sales_selling_price', 'item_purchase_selling_price', 'item_sales_account', 'item_purchase_account', 'item_sales_description', 'item_purchase_description', 'item_sales_tax', 'item_purchase_preferred_vendor', 'item_reorder_level', 'item_opening_stock', 'item_opening_stock_value'];
    protected $dates = ['deleted_at'];

    public function setItemNameAttribute($value)
    {
        $this->attributes['item_name'] = ($value);
    }

    public function setItemSkuAttribute($value)
    {
        $this->attributes['item_sku'] = ($value);
    }
    public function setItemUnitAttribute($value)
    {
        $this->attributes['item_unit'] = ($value);
    }
    public function setItemSalesSellingPriceAttribute($value)
    {
        $this->attributes['item_sales_selling_price'] = ($value);
    }
    public function setItemPurchaseSellingPriceAttribute($value)
    {
        $this->attributes['item_purchase_selling_price'] = ($value);
    }
    public function setItemSalesAccountAttribute($value)
    {
        $this->attributes['item_sales_account'] = ($value);
    }
    public function setItemPurchaseAccountAttribute($value)
    {
        $this->attributes['item_purchase_account'] = ($value);
    }
    public function setItemSalesDescriptionAttribute($value)
    {
        $this->attributes['item_sales_description'] = ($value);
    }
    public function setItemPurchaseDescriptionAttribute($value)
    {
        $this->attributes['item_purchase_description'] = ($value);
    }
    public function setItemSalesTaxAttribute($value)
    {
        $this->attributes['item_sales_tax'] = ($value);
    }
    public function setItemPurchasePreferredVendorAttribute($value)
    {
        $this->attributes['item_purchase_preferred_vendor'] = ($value);
    }
    public function setItemReorderLevelAttribute($value)
    {
        $this->attributes['item_reorder_level'] = ($value);
    }
    public function setItemOpeningStockAttribute($value)
    {
        $this->attributes['item_opening_stock'] = ($value);
    }
    public function setItemOpeningStockValueAttribute($value)
    {
        $this->attributes['item_opening_stock_value'] = ($value);
    }
}