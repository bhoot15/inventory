<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Supplier extends Model
{
    use SoftDeletes;
    protected $table = "suppliers";
    protected $fillable = ['supplier_name','company_name','contact_email','contact_phone','supplier_website','currency','payment_terms','billing_house','billing_street','billing_city','billing_zip','billing_country','billing_phone','shipping_house','shipping_street','shipping_city','shipping_zip','shipping_country','shipping_phone','note'];
    protected $dates = ['deleted_at'];

    public function setSupplierNameAttribute($value){
        $this->attributes['supplier_name'] = ($value);
    }
    public function setCompanyNameAttribute($value){
        $this->attributes['company_name'] = ($value);
    }
    public function setContactEmailAttribute($value){
        $this->attributes['contact_email'] = ($value);
    }
    public function setContactPhoneAttribute($value){
        $this->attributes['contact_phone'] = ($value);
    }
    public function setSupplierWebsiteAttribute($value){
        $this->attributes['supplier_website'] = ($value);
    }
    public function setCurrencyAttribute($value){
        $this->attributes['currency'] = ($value);
    }
    public function setPaymentTermsAttribute($value){
        $this->attributes['payment_terms'] = ($value);
    }
    public function setBillingHouseAttribute($value){
        $this->attributes['billing_house'] = ($value);
    }
    public function setBillingStreetAttribute($value){
        $this->attributes['billing_street'] = ($value);
    }
    public function setBillingCityAttribute($value){
        $this->attributes['billing_city'] = ($value);
    }
    public function setBillingZipAttribute($value){
        $this->attributes['billing_zip'] = ($value);
    }
    public function setBillingCountryAttribute($value){
        $this->attributes['billing_country'] = ($value);
    }
	  public function setBillingPhoneAttribute($value){
        $this->attributes['billing_phone'] = ($value);
    }
	 public function setShippingHouseAttribute($value){
        $this->attributes['shipping_house'] = ($value);
    }
    public function setShippingStreetAttribute($value){
        $this->attributes['shipping_street'] = ($value);
    }
    public function setShippingCityAttribute($value){
        $this->attributes['shipping_city'] = ($value);
    }
    public function setShippingZipAttribute($value){
        $this->attributes['shipping_zip'] = ($value);
    }
    public function setShippingCountryAttribute($value){
        $this->attributes['shipping_country'] = ($value);
    }
	  public function setShippingPhoneAttribute($value){
        $this->attributes['shipping_phone'] = ($value);
    }
    public function setNoteAttribute($value){
        $this->attributes['note'] = ($value);
    }
   


}

