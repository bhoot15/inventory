<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Uom extends Model
{
    use SoftDeletes;
    protected $table = "uoms";
    protected $fillable = ['item_attribute'];
    protected $dates = ['deleted_at'];

    public function setItemAttributeAttribute($value)
    {
        $this->attributes['item_attribute'] = ($value);
    }
}