<?php


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::resource('/item', 'ItemController');
Route::resource('/uom', 'UnitController');
Route::resource('/supplier', 'SupplierController');
Route::resource('/purchase', 'PurchaseController');
Route::resource('/customer', 'CustomerController');
Route::resource('/sales', 'SalesController');
Route::get('/purchase-outstanding','PurchaseController@purchaseOutstanding');
Route::get('/purchase/{id}/print', 'PurchaseController@purchasePrint');
Route::get('/invoice/{id}/print', 'InvoiceController@invoicePrint');
Route::get('/sales-outstanding', 'SalesController@salesOutstanding');
Route::get('/payment-transaction','HomeController@paymentTransaction');
Route::resource('/invoices', 'InvoiceController');
Route::resource('/bills', 'BillController');

