DELIMITER $$

USE `inventory`$$

DROP TRIGGER /*!50032 IF EXISTS */ `sales_outsanding_insert`$$

CREATE
   /*!50017 DEFINER = 'root'@'localhost' */
   TRIGGER `sales_outsanding_insert` AFTER INSERT ON `sales`
   FOR EACH ROW BEGIN
   IF new.total_balance>0 THEN
INSERT INTO sales_outstandings (id, sales_id, sales_date, customer_id, notes, total_amount, total_payment, total_balance, created_at, updated_at)
VALUES(new.id, new.sales_number, NOW(), new.customer_id, new.notes, new.total_amount, new.total_payment, new.total_balance,NOW(),NOW());
END IF;
   END;
$$

DELIMITER ;