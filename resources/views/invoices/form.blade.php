<style>
    p {
        color: #ff0000;
    }
</style>
<div class="row">

    <div class="col-sm-4">
        <div class="form-group">
            <label>Invoice No.</label>
            <input type="text" class="form-control" v-model="form.invoice_no">
            <p v-if="errors.invoice_no" class="error">@{{errors.invoice_no[0]}}</p>
        </div>
        <div class="form-group">
            <label>Customer Name</label>
            <select class="form-control" name="client" v-model="form.client">
                <option disabled>Please Select Customer</option>
                @foreach($customers as $customer)
                    <option value="{{$customer->customer_name}}">{{$customer->customer_name}}</option>
                @endforeach
            </select>
            <p v-if="errors.client" class="error">@{{errors.client[0]}}</p>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Customer Address</label>
            <textarea class="form-control" v-model="form.client_address"></textarea>
            <p v-if="errors.client_address" class="error">@{{errors.client_address[0]}}</p>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Sales Person</label>
            <select class="form-control" name="supplier_name" v-model="form.sales_person">
                <option disabled>Select Supplier</option>
                @foreach($suppliers as $supplier)
                    <option value="{{$supplier->supplier_name}}">{{$supplier->supplier_name}}</option>
                @endforeach
            </select>
            <p v-if="errors.sales_person" class="error">@{{errors.sales_person[0]}}</p>
        </div>
        <div class="form-group">
            <label>Order Number</label>
            <input type="text" class="form-control" v-model="form.order_number">
            <p v-if="errors.order_number" class="error">@{{errors.order_number[0]}}</p>
        </div>
        <div class="form-group">
            <label>Payment Terms</label>
            <select class="form-control" name="terms" v-model="form.terms">
                <option disabled>Select a Payment Term</option>
                <option>7 days</option>
                <option>10 days</option>
                <option>15 days</option>
                <option>20 days</option>
                <option>30 days</option>
                <option>3 months</option>
                <option>6 months</option>
                <option>1 year</option>
            </select>
            <p v-if="errors.terms" class="error">@{{errors.terms[0]}}</p>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <label>Invoice Date</label>
                <input type="date" class="form-control" v-model="form.invoice_date">
                <p v-if="errors.invoice_date" class="error">@{{errors.invoice_date[0]}}</p>
            </div>
            <div class="col-sm-6">
                <label>Due Date</label>
                <input type="date" class="form-control" v-model="form.due_date">
                <p v-if="errors.due_date" class="error">@{{errors.due_date[0]}}</p>
            </div>
        </div>
    </div>
</div>
<hr>
<div v-if="errors.products_empty">
    <p class="alert alert-danger">@{{errors.products_empty[0]}}</p>
    <hr>
</div>
<table class="table table-bordered table-form">
    <thead>
        <tr>
            <th>Product Name</th>
            <th>Price</th>
            <th>Qty</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="product in form.products">
            <td class="table-name" :class="{'table-error': errors['products.' + $index + '.name']}">
                {{--<textarea class="table-control" v-model="product.name"></textarea>--}}

                <select class="form-control" id="name" v-model="product.name">
                    <option disabled>Please Select Item</option>
                    @foreach($items as $item)
                        <option @click="selectItem" value="{{$item->item_sales_selling_price}}"
                        >{{$item->item_name}}</option>
                    @endforeach
                </select>
            </td>
            <td class="table-price" :class="{'table-error': errors['products.' + $index + '.price']}">
                {{--<input type="text" class="table-control"  v-model="product.price">--}}
                <input type="text" class="form-control" v-model="product.price" id="price">
            </td>
            <td class="table-qty" :class="{'table-error': errors['products.' + $index + '.qty']}">
                <input type="text" class="form-control" v-model="product.qty">
            </td>
            <td class="table-total">
                <span class="table-text" id="total">@{{product.qty * product.price}}</span>
            </td>
            <td class="table-remove">
                <span @click="remove(product)" class="table-remove-btn"><i class="fa fa-trash-o"
                                                                           aria-hidden="true"></i></span>
            </td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td class="table-empty" colspan="2">
                <span @click="addLine" class="table-add_line"><font style="cursor: pointer; color: #00c0ef">Add
                    Line</font></span>
            </td>
            <td class="table-label">Sub Total</td>
            <td class="table-amount">@{{subTotal}}</td>
        </tr>
        <tr>
            <td class="table-empty" colspan="2"></td>
            <td class="table-label">Discount</td>
            <td class="table-discount" :class="{'table-error': errors.discount}">
                <input type="text" class="table-discount_input" v-model="form.discount">
            </td>
        </tr>
        <tr>
            <td class="table-empty" colspan="2"></td>
            <td class="table-label">Grand Total</td>
            <td class="table-amount">@{{grandTotal}}</td>
        </tr>
    </tfoot>
</table>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 pull-right">
        <div class="form-group">
            <label>Sales Notes</label>
            <textarea class="form-control" rows="3"
                      placeholder="Notes"
                      name="notes" v-model="form.notes"></textarea>
            <p v-if="errors.notes" class="error">@{{errors.notes[0]}}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 pull-right">
        <div class="form-group">
            <label>Terms & Condition</label>
            <textarea class="form-control" rows="3"
                      placeholder="Terms & Condition"
                      name="tnc" v-model="form.tnc"></textarea>
            <p v-if="errors.tnc" class="error">@{{errors.tnc[0]}}</p>
        </div>
    </div>
</div>