@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Invoices
            <small>list</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Invoices</li>
        </ol>
    </section>

    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="clearfix">
                    <a href="{{route('invoices.create')}}" class="btn btn-info pull-left"><i class="fa fa-plus"
                                                                                             aria-hidden="true"></i> New</a>
                </div>
            </div>
            <div class="panel-body">
                @if($invoices->count())
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Invoice No.</th>
                            <th>Grand Total</th>
                            <th>Client</th>
                            <th>Invoice Date</th>
                            <th>Due Date</th>
                            <th colspan="2">Created At</th>
                            <th colspan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoices as $invoice)
                            <tr>
                                <td>{{$invoice->invoice_no}}</td>
                                <td>‎৳ {{$invoice->grand_total}}</td>
                                <td>{{$invoice->client}}</td>
                                <td>{{$invoice->invoice_date}}</td>
                                <td>{{$invoice->due_date}}</td>
                                <td>{{$invoice->created_at->diffForHumans()}}</td>
                                <td class="text-left">
                                    <a href="{{route('invoices.show', $invoice)}}"
                                       class="btn btn-success btn-sm"><i class="fa fa-search" aria-hidden="true"></i>
                                        View</a>
                                </td>
                                <td>
                                    <a href="{{route('invoices.edit', $invoice)}}"
                                       class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                         aria-hidden="true"></i> Edit</a>
                                </td>
                                <td>
                                    <form class="form-inline" method="post"
                                          action="{{route('invoices.destroy', $invoice)}}"
                                          onsubmit="return confirm('Are you sure?')"
                                            >
                                        <input type="hidden" name="_method" value="delete">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button class="btn btn-danger btn-sm">
                                            <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $invoices->render() !!}
                @else
                    <div class="invoice-empty">
                        <p class="invoice-empty-title">
                            No Invoices were created.
                            <a href="{{route('invoices.create')}}">Create Now!</a>
                        </p>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection