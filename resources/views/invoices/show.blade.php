@extends('layouts.app')

@section('content')

    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="clearfix">
                    <span class="panel-title">Invoice</span>

                    <div class="pull-right">
                        {{--<a href="{{route('purchases.index')}}" class="btn btn-default">Back</a>--}}
                        <table class="table-responsive">
                            <tr>
                                <td><a href="/invoice" class="btn btn-info"><i class="fa fa-arrow-left"
                                                                               aria-hidden="true"></i> Back </a></td>
                                <td><a href="/invoice/{{$invoice->id}}/edit" class="btn btn-primary">Edit</a></td>
                                <td>
                                    <form class="form-inline" onsubmit="return confirm('Are you sure?')"
                                          action="/invoice/{{$invoice->id}}" method="post" class="pull-left">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button class="btn btn-danger">
                                            <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                                <td>
                                    <a target="_blank" href="/invoice/{{$invoice->id}}/print" target="_blank"
                                       class="btn btn-success pull-right"><i class="fa fa-print"></i>
                                        Print</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-2">
                        <br><img class="img-responsive" src="/dist/img/logo-black.png">
                    </div>
                    <div class="col-xs-8 text-center">
                        <h3 style="color: #00c0ef;">
                            @foreach ($supplier as $object)
                                {{--{{ $object->company_name }}--}}
                                {{ ucwords(trans($object->company_name)) }}
                        </h3>
                        <h4>House# {{ $object->billing_house }}, Street# {{ $object->billing_street }}</h4>
                        <h4>City# {{ $object->billing_city }} , Zip# {{ $object->billing_zip }}
                            , {{$object->billing_country}}</h4>
                        <h4>Phone: {{$object->billing_phone}} ,
                            website:
                            <a target="_blank">{{ $object->supplier_website }}</a>
                            @endforeach
                        </h4>
                    </div>
                    <!-- /.col -->
                </div>
                <br><br>

                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        Bill To
                        <address>
                            <strong>{{$invoice->client}}</strong>
                            <br>
                            @foreach ($customer as $co)
                                House# {{ $co->billing_house }} , Street# {{ $co->billing_street }}

                                <br>
                                City# {{ $co->billing_city }} , Zip# {{ $co->billing_zip }}
                                , {{$co->billing_country}}
                                <br>
                                Phone: {{$co->billing_phone}}<br>
                                Email: {{$co->contact_email}}
                            @endforeach
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Shift To
                        <address>
                            <strong>{{$invoice->client}}</strong>
                            <br>
                            @foreach ($customer as $co)
                                House# {{ $co->shipping_house }} , Street# {{ $co->shipping_street }}

                                <br>
                                City# {{ $co->shipping_city }} , Zip# {{ $co->shipping_zip }}
                                , {{$co->shipping_country}}
                                <br>
                                Phone: {{$co->shipping_phone}}<br>
                            @endforeach
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <b>#Purchase - {{$invoice->purchase_no}}</b><br>
                        <br>
                        <b>Order ID:</b> {{$invoice->purchase_order_number}}<br>
                        <b>Payment Due:</b> {{$invoice->due_date}}<br>
                        <b>Purchase Reference:</b> {{$invoice->purchase_reference}}
                    </div>
                    <!-- /.col -->
                </div>

                <hr>

                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invoice->products as $product)
                        <tr>
                            <td class="table-name">{{$product->name}}</td>
                            <td class="table-price">${{$product->price}}</td>
                            <td class="table-qty">{{$product->qty}}</td>
                            <td class="table-total"> ৳ {{$product->qty * $product->price}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Sub Total</td>
                        <td class="table-amount"> ৳ {{$invoice->sub_total}}</td>
                    </tr>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Discount</td>
                        <td class="table-amount"> ৳ {{$invoice->discount}}</td>
                    </tr>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Grand Total</td>
                        <td class="table-amount"> ৳ {{$invoice->grand_total}}</td>
                    </tr>
                    </tfoot>
                </table>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-8 form-group pull-left">
                            <br>

                            <p>
                                Thank you for your business!
                                <br>
                                Should you have any enquiries concerning this invoice, please contact
                                <br>
                                <b>{{$invoice->supplier_name}}</b>
                                <br>
                                @foreach($supplier as $object)
                                    Voice: {{$object->contact_phone}}
                                    <br>
                                    Email: {{$object->contact_email}}
                                    <br>
                                    {{ ucwords(trans($object->company_name)) }}
                                    <br>
                                    House# {{ $object->billing_house }}, Street# {{ $object->billing_street }}
                                    City# {{ $object->billing_city }} , Zip# {{ $object->billing_zip }}
                                    , {{$object->billing_country}}
                                    <br>
                                    Visit Us:
                                    <a>{{ $object->supplier_website }}</a>
                                @endforeach
                            </p>
                        </div>

                        <div class="col-md-4 form-group pull-right">
                            <br><label>___________________________</label> <br>

                            <small class="text-center">Authorized Signature & Date</small>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <a target="_blank" href="/invoice/{{$invoice->id}}/print" target="_blank"
                   class="btn btn-success pull-right"><i class="fa fa-print"></i>
                    Print</a>
            </div>
        </div>
    </section>


@endsection