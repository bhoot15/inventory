@extends('layouts.app')

@section('content')
    <section class="content">
        <div id="purchase">
            <div class="panel panel-default" v-cloak>
                <div class="panel-heading">
                    <div class="clearfix">
                        <span class="panel-title">Edit Purchase</span>
                        <a href="/purchase" class="btn btn-default pull-right">Back</a>
                    </div>
                </div>
                <div class="panel-body">
                    @include('purchases.form')
                </div>
                <div class="panel-footer">
                    <a href="/purchase" class="btn btn-default">CANCEL</a>
                    <button class="btn btn-success" @click="update" :disabled="isProcessing">UPDATE</button>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>
<script src="/js/vue.min.js"></script>
<script src="/js/vue-resource.min.js"></script>
<script type="text/javascript">
    Vue.http.headers.common['X-CSRF-TOKEN'] = '{{csrf_token()}}';
    /*window._form = {!!$purchase -> toJson()!!}; //this is correct format*/
    window._form = {
    !!$purchase - > toJson()
    !!
    }
    ;
</script>
<script src="/js/purchase.js"></script>
@endpush