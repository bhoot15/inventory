<style>
    p {
        color: #ff0000;
    }
</style>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Supplier Name</label>
            {{--<input type="text" name="supplier_name" class="form-control" v-model="form.supplier_name"
                   placeholder="">--}}

            <select class="form-control" name="supplier_name" v-model="form.supplier_name">
                <option disabled>Select Supplier</option>
                @foreach($suppliers as $supplier)
                    <option value="{{$supplier->supplier_name}}">{{$supplier->supplier_name}}</option>
                @endforeach
            </select>
            <p v-if="errors.supplier_name" class="error">@{{errors.supplier_name[0]}}</p>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Purchase Order</label>
            <input type="text" name="purchase_order_number" class="form-control"
                   placeholder="" v-model="form.purchase_order_number">
            <p v-if="errors.purchase_order_number" class="error">@{{errors.purchase_order_number[0]}}</p>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Reference</label>
            <input type="text" name="purchase_reference" class="form-control"
                   placeholder="" v-model="form.purchase_reference">
            <p v-if="errors.purchase_reference" class="error">@{{errors.purchase_reference[0]}}</p>
        </div>
        {{--<div class="form-group">
            <label>Shipment Preference</label>
            <select class="form-control" name="">
                <option disabled selected>Select or Type to add</option>
                <option>something</option>
            </select>
        </div>--}}

        <div class="form-group">
            <label>Purchase No.</label>
            <input type="text" class="form-control" v-model="form.purchase_no">
            <p v-if="errors.purchase_no" class="error">@{{errors.purchase_no[0]}}</p>
        </div>
        <div class="form-group">
            <label>Customer</label>
            {{--<input type="text" class="form-control" v-model="form.client">--}}
            <select class="form-control" name="client" v-model="form.client">
                <option disabled>Please Select Customer</option>
                @foreach($customers as $customer)
                    <option value="{{$customer->customer_name}}">{{$customer->customer_name}}</option>
                @endforeach
            </select>
            <p v-if="errors.client" class="error">@{{errors.client[0]}}</p>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Customer Address</label>
            <textarea class="form-control" v-model="form.client_address"></textarea>
            <p v-if="errors.client_address" class="error">@{{errors.client_address[0]}}</p>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" v-model="form.title">
            <p v-if="errors.title" class="error">@{{errors.title[0]}}</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <label>Purchase Date</label>
                <input type="date" class="form-control" v-model="form.purchase_date">
                <p v-if="errors.purchase_date" class="error">@{{errors.purchase_date[0]}}</p>
            </div>
            <div class="col-sm-6">
                <label>Due Date</label>
                <input type="date" class="form-control" v-model="form.due_date">
                <p v-if="errors.due_date" class="error">@{{errors.due_date[0]}}</p>
            </div>
        </div>
    </div>
    <hr>
    <div v-if="errors.products_empty">
        <p class="alert alert-danger">@{{errors.products_empty[0]}}</p>
        <hr>
    </div>
    <table class="table table-bordered table-form">
        <thead>
        <tr>
            <th>Product Name</th>
            <th>Price</th>
            <th>Qty</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="product in form.products">
            <td class="table-name" :class="{'table-error': errors['products.' + $index + '.name']}">
                {{--<textarea class="table-control" v-model="product.name"></textarea>--}}

                <select class="form-control" id="name" v-model="product.name">
                    <option disabled>Please Select Item</option>
                    @foreach($items as $item)
                        <option @click="selectItem" value="{{$item->item_sales_selling_price}}"
                        >{{$item->item_name}}</option>
                    @endforeach
                </select>
            </td>
            <td class="table-price" :class="{'table-error': errors['products.' + $index + '.price']}">
                {{--<input type="text" class="table-control"  v-model="product.price">--}}
                <input type="text" class="form-control" v-model="product.price" id="price">
            </td>
            <td class="table-qty" :class="{'table-error': errors['products.' + $index + '.qty']}">
                <input type="text" class="form-control" v-model="product.qty">
            </td>
            <td class="table-total">
                <span class="table-text" id="total">@{{product.qty * product.price}}</span>
            </td>
            <td class="table-remove">
                <span @click="remove(product)" class="table-remove-btn"><i class="fa fa-trash-o"
                                                                           aria-hidden="true"></i></span>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td class="table-empty" colspan="2">
                <span @click="addLine" class="table-add_line"><font style="cursor: pointer; color: #00c0ef">Add
                    Line</font></span>
            </td>
            <td class="table-label">Sub Total</td>
            <td class="table-amount">@{{subTotal}}</td>
        </tr>
        <tr>
            <td class="table-empty" colspan="2"></td>
            <td class="table-label">Discount</td>
            <td class="table-discount" :class="{'table-error': errors.discount}">
                <input type="text" class="table-discount_input" v-model="form.discount">
            </td>
        </tr>
        <tr>
            <td class="table-empty" colspan="2"></td>
            <td class="table-label">Grand Total</td>
            <td class="table-amount">@{{grandTotal}}</td>
        </tr>
        </tfoot>
    </table>
    <div class="content">
        <div class="row">
            <div class="col-md-6 pull-right">
                <div class="form-group">
                    <label>Sales Notes</label>
                    <textarea class="form-control" rows="3"
                              placeholder="Notes"
                              name="sales_note" v-model="form.sales_note"></textarea>

                    <p v-if="errors.sales_note" class="error">@{{errors.sales_note[0]}}</p>
            </div>
                <div class="form-group">
                    <label>Terms & Condition</label>
                    <textarea class="form-control" rows="3"
                              placeholder="Terms & Condition"
                              name="sales_tnc" v-model="form.sales_tnc"></textarea>

                    <p v-if="errors.sales_tnc" class="error">@{{errors.sales_tnc[0]}}</p>
                </div>
        </div>
        </div>
    </div>
</div>
