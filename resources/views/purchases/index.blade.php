@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Purchases
            <small>list</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Purchases</li>
        </ol>
    </section>

    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="clearfix">
                    <a href="/purchase/create" class="btn btn-info pull-left"><i class="fa fa-plus"
                                                                                 aria-hidden="true"></i> New</a>
                </div>
            </div>
            <div class="panel-body">
                @if($purchases->count())
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Purchase No.</th>
                            <th>Grand Total</th>
                            <th>Client</th>
                            <th>Purchase Date</th>
                            <th>Due Date</th>
                            <th colspan="2">Created At</th>
                            <th colspan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($purchases as $purchase)
                            <tr>
                                <td>{{$purchase->purchase_no}}</td>
                                <td>‎৳ {{$purchase->grand_total}}</td>
                                <td>{{$purchase->client}}</td>
                                <td>{{$purchase->purchase_date}}</td>
                                <td>{{$purchase->due_date}}</td>
                                <td>{{$purchase->created_at->diffForHumans()}}</td>
                                <td class="text-left">
                                    <a href="/purchase/{{$purchase->id}}"
                                       class="btn btn-success btn-sm"><i class="fa fa-search" aria-hidden="true"></i>
                                        View</a>
                                </td>
                                <td>
                                    <a href="/purchase/{{$purchase->id}}/edit"
                                       class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                         aria-hidden="true"></i> Edit</a>
                                </td>
                                <td>
                                    <form class="form-inline" onsubmit="return confirm('Are you sure?')"
                                          action="/purchase/{{$purchase->id}}" method="post" class="pull-left">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button class="btn btn-danger btn-sm">
                                            <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                    {{--<form class="form-inline" method="post"
                                          action="{{route('purchases.destroy', $purchase)}}"
                                          onsubmit="return confirm('Are you sure?')"
                                            >
                                        <input type="hidden" name="_method" value="delete">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button class="btn btn-danger btn-sm">
                                            <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $purchases->render() !!}
                @else
                    <div class="invoice-empty">
                        <p class="invoice-empty-title">
                            No Purchases were created.
                            <a href="/purchase/create">Create Now!</a>
                        </p>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection