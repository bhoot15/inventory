@extends('layouts.app')

@section('content')
    <div class="content">
        <!-- Content Header (Page header) -->
        <form role="form" action="/item" method="post">
            {{csrf_field()}}
            <section class="content-header">
                <h1>
                    New Item
<!--                    <small>Create</small>-->
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="/item">Items</a></li>
                    <li class="active">Create</li>
                </ol>
            </section>

            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Create Item</h3>

                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Item Name</label>
                                                <input type="text" name="item_name" class="form-control"
                                                       placeholder="Item Name">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">SKU</label>
                                                <input type="text" name="item_sku" class="form-control"
                                                       placeholder="SKU">
                                            </div>
                                            <div class="form-group">
                                                <label>Unit</label>
                                                <select class="form-control" name="item_unit">
                                                    <option disabled selected>Select to add unit</option>
                                                    <option>kg</option>
                                                    <option>litre</option>
                                                    <option>pcs</option>
                                                    <option>dozen</option>
                                                    <option >+ Add New</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box box-danger">
                            <div class="box-header">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <h3 class="box-title">Sales info</h3><br><br>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Selling Price</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Selling Price"
                                                               name="item_sales_selling_price">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <textarea class="form-control" rows="3"
                                                                  placeholder="Description"
                                                                  name="item_sales_description"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tax</label>
                                                        <select class="form-control" name="item_sales_tax">
                                                            <option disabled selected>Select a Tax</option>
                                                            <option>15%</option>
                                                            <option>Add new</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <h3 class="box-title">Purchase info</h3><br><br>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Purchase Price</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Selling Price"
                                                               name="item_purchase_selling_price">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <textarea class="form-control" rows="3"
                                                                  placeholder="Description"
                                                                  name="item_purchase_description"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Preferred Supplier</label>
                                                        <select class="form-control"
                                                                name="item_purchase_preferred_vendor">
                                                            <option disabled selected>Select a Supplier</option>
                                                            <option>ACI</option>
                                                            <option>Add new</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title">Track inventory item</h3>

                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Reorder level</label>
                                                <input type="text" name="item_reorder_level" class="form-control"
                                                       placeholder="Reorder level">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Opening stock</label>
                                                <input type="text" name="item_opening_stock" class="form-control"
                                                       placeholder="Opening Stock">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Opening stock value</label>
                                                <input type="text" name="item_opening_stock_value" class="form-control"
                                                       placeholder="Opening stock value">
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="box-footer">
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                            <button type="submit" class="btn btn-info">&nbsp;Save&nbsp;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </section>

        </form>
    </div>



@endsection
