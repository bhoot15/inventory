@extends('layouts.app')

@section('content')

<div class="content">
    <!-- Content Header (Page header) -->
    <form action="/item/{{$item->id}}" method="post">
        {{method_field('PUT')}}
        {{csrf_field()}}
        <section class="content-header">
            <h1>
                Item Categories
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="/item">Items</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Edit Item</h3>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Item Name</label>
                                            <input type="text" name="item_name" class="form-control"
                                                   value="{{$item->item_name}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">SKU</label>
                                            <input type="text" name="item_sku" class="form-control"
                                                   value="{{$item->item_sku}}" >
                                        </div>
                                        <div class="form-group">
                                            <label>Unit</label>
                                            <select class="form-control" name="item_unit">
                                                <option disabled selected>{{$item->item_unit}}</option>

                                                @foreach($units as $unit)
                                                <option>{{$unit->item_attribute}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3 class="box-title">Sales info</h3><br><br>

                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Selling Price</label>
                                                    <input type="text" class="form-control"
                                                           value="{{$item->item_sales_selling_price}}"
                                                           name="item_sales_selling_price">
                                                </div>
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <textarea class="form-control" rows="3"
                                                              name="item_sales_description">{{$item->item_sales_description}}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tax</label>
                                                    <select class="form-control" name="item_sales_tax">
                                                        <option disabled selected>{{$item->item_sales_tax}}</option>
                                                        <option>15%</option>
                                                        <!--                                                        <option>Add new</option>-->
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3 class="box-title">Purchase info</h3><br><br>

                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Purchase Price</label>
                                                    <input type="text" class="form-control"
                                                           value="{{$item->item_purchase_selling_price}}"
                                                           name="item_purchase_selling_price">
                                                </div>
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <textarea class="form-control" rows="3"

                                                              name="item_purchase_description">{{$item->item_purchase_description}}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Preferred Supplier</label>
                                                    <select class="form-control"
                                                            name="item_purchase_preferred_vendor">
                                                        <option disabled selected>{{$item->item_purchase_preferred_vendor}}</option>
                                                        @foreach($suppliers as $supplier)
                                                        <option>{{$supplier->supplier_name}}</option>
                                                        @endforeach
                                                        <!--                                                        <option>Add new</option>-->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title">Track inventory item</h3>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Reorder level</label>
                                            <input type="text" name="item_reorder_level" class="form-control"
                                                   value="{{$item->item_reorder_level}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Opening stock</label>
                                            <input type="text" name="item_opening_stock" class="form-control"
                                                   value="{{$item->item_opening_stock}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Opening stock value</label>
                                            <input type="text" name="item_opening_stock_value" class="form-control"
                                                   value="{{$item->item_opening_stock_value}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="box-footer">
                                        <button type="cancel" class="btn btn-default">Cancel</button>
                                        <button type="submit" class="btn btn-info">&nbsp;Save&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </section>
    </form>
</div>
@endsection
