@extends('layouts.app')

@section('content')
<div class="content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Item
            <small>Lists</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Item</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <!--<h3 class="box-title">item Items</h3>-->
                        <div class="row">
                            <div class="box-body">
                                <div class="pull-left">
                                    <button type="button" class="btn btn-info" data-toggle="modal"
                                            data-target="#myModal"><i class="fa fa-btn fa-plus" aria-hidden="true"></i>
                                        NEW
                                    </button>
                                    <br><br>
                                    <!--<a href="#" class="btn btn-danger" role="button"><i class="fa fa-btn fa-trash"
                                                                                    aria-hidden="true"></i></a>-->
                                </div>
                                <div class="pull-right">
                                    <div class="box-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control"
                                                   placeholder="Search">

                                            <div class="input-group-btn">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Name</th>
                                        <th>SKU</th>
                                        <th>Available</th>
                                        <th>Reorder level</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    @foreach($items as $item)
                                    @if(!$item->deleted_at)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->item_name}}</td>
                                        <td>{{$item->item_sku}}</td>
                                        <td>{{$item->item_opening_stock}}</td>
                                        <td>{{$item->item_reorder_level}}</td>
                                        {{--
                                        <td>
                                            <a href="/item/{{$item->id }}"><i class="fa fa-btn fa-search"
                                                                              aria-hidden="true"></i>View</a>
                                        </td>
                                        --}}
                                        <td>
                                            <a href="/item/{{$item->id}}/edit"><i class="fa fa-btn fa-pencil-square-o"
                                                                                  aria-hidden="true"></i>Edit</a>
                                        </td>
                                        <td>
                                            <form action="item/{{$item->id}}" method="post" class="pull-left">
                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}
                                                <button class="btn btn-danger btn-sm">
                                                    <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                    @endif
                                    @endforeach


                                </table>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>
{{--Item create modal Starts--}}
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Items</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="/item" method="post">
                    {{csrf_field()}}
                    {{--
                    <section class="content-header">
                        <h1>
                            Suppliers
                            <small>Create</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li><a href="/supplier">Suppliers</a></li>
                            <li class="active">Create</li>
                        </ol>
                    </section>
                    --}}


                    <section class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title">Item Info</h3>

                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-lg-9">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Item Name</label>
                                                        <input type="text" name="item_name" class="form-control"
                                                               placeholder="Item Name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">SKU</label>
                                                        <input type="text" name="item_sku" class="form-control"
                                                               placeholder="SKU">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Unit</label>
                                                        <select class="form-control" name="item_unit" id="myselect">
                                                            <option disabled selected>Select to add unit</option>

                                                            @foreach($units as $unit)
                                                            <option>{{$unit->item_attribute}}</option>
                                                            @endforeach
                                                            <option value="secondoption">+ Add New</option>
                                                            <!--<option value="secondoption">+ Add New</option>-->
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="box box-danger">
                                    <div class="box-header">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <h3 class="box-title">Sales info</h3> <br><br>

                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Selling Price</label>
                                                                <input type="text" class="form-control"
                                                                       placeholder="Selling Price"
                                                                       name="item_sales_selling_price">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Description</label>
                                                                <textarea class="form-control" rows="3"
                                                                          placeholder="Description"
                                                                          name="item_sales_description"></textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Tax</label>
                                                                <select class="form-control" name="item_sales_tax">
                                                                    <option disabled selected>Select a Tax</option>
                                                                    <option>15%</option>
                                                                    <!--<option>Add new</option>-->
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <h3 class="box-title">Purchase info</h3><br><br>

                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Purchase Price</label>
                                                                <input type="text" class="form-control"
                                                                       placeholder="Selling Price"
                                                                       name="item_purchase_selling_price">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Description</label>
                                                                <textarea class="form-control" rows="3"
                                                                          placeholder="Description"
                                                                          name="item_purchase_description"></textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Preferred Supplier</label>
                                                                <select class="form-control"
                                                                        name="item_purchase_preferred_vendor">
                                                                    <option disabled selected>Select Prefered Supplier
                                                                    </option>
                                                                    @foreach($suppliers as $supplier)
                                                                    <option>{{$supplier->supplier_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="box box-success">
                                    <div class="box-header">
                                        <h3 class="box-title">Track inventory item</h3>

                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-lg-9">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Reorder level</label>
                                                        <input type="text" name="item_reorder_level"
                                                               class="form-control"
                                                               placeholder="Reorder level">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Opening stock</label>
                                                        <input type="text" name="item_opening_stock"
                                                               class="form-control"
                                                               placeholder="Opening Stock">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Opening stock value</label>
                                                        <input type="text" name="item_opening_stock_value"
                                                               class="form-control"
                                                               placeholder="Opening stock value">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </section>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="box-footer">
                        <button type="reset" class="btn btn-default pull-left">Reset</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">&nbsp;Save&nbsp;</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->

<!--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>-->
{{--add unit modal--}}
<div class="modal fade" id="myModalUnit" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Itrem Attribute</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="/uom" method="post">
                    {{csrf_field()}}

                    <section class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-lg-9">
                                                    <!--<div class="form-group">
                                                        <label for="exampleInputEmail1">Item Name</label>
                                                        <input type="text" name="item_name" class="form-control"
                                                               placeholder="Item Name">
                                                    </div>-->
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Item Attribute/Unit</label>
                                                        <input type="text" name="item_attribute" class="form-control"
                                                               placeholder="Item Attribute/Unit">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="box-footer">
                                    <button type="reset" class="btn btn-default pull-left">Reset</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-success">&nbsp;Save&nbsp;</button>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
<script type="text/javascript">
    $('#myselect').change(function () {
        var opval = $(this).val();
        if (opval == "secondoption") {
            $('#myModalUnit').modal("show");
        }
    });
</script>
@endpush
