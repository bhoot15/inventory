@extends('layouts.app')

@section('content')

<div class="content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Item Details
            <small>preview of items</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/uom">Unit of Management</a></li>
            <li class="active">Show Details</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-9 col-lg-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Unit Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection