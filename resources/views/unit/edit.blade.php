@extends('layouts.app')

@section('content')

<div class="content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Stock Categories
            <small>Edit</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/uom">UOM</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Categories</h3>
                </div>
                <form role="form" action="/uom/{{$unit->id}}" method="post">
                    {{method_field('PUT')}}
                    {{csrf_field()}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Item Attribute/Unit</label>
                            <input type="text" name="item_attribute" class="form-control"
                                   value="{{$unit->item_attribute}}"
                                   placeholder="Item Attribute/Unit">
                        </div>
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="submit" class="btn btn-info">&nbsp;Save&nbsp;</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>
</div>
@endsection
