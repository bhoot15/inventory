@extends('layouts.app')

@section('content')
<div class="content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Stock Categories
            <small>Create</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/uom">UOM</a></li>
            <li class="active">Create</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Create Item Unit</h3>
                    </div>
                    <div class="box-body">
                        <p>Unit Name</p>

                        <div class="input-group margin">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


@endsection
