@extends('layouts.app')

@section('content')
<div class="content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Item Attribute
            <small>Lists</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Item Attributes</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <!--<h3 class="box-title">item Items</h3>-->
                        <div class="row">
                            <div class="box-body">
                                <div class="pull-left">
                                    <button type="button" class="btn btn-info" data-toggle="modal"
                                            data-target="#myModal"><i class="fa fa-btn fa-plus" aria-hidden="true"></i>
                                        NEW
                                    </button>
                                    <br><br>
                                    <!--<a href="#" class="btn btn-danger" role="button"><i class="fa fa-btn fa-trash"
                                                                                    aria-hidden="true"></i></a>-->
                                </div>
                                <div class="pull-right">
                                    <div class="box-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control"
                                                   placeholder="Search">

                                            <div class="input-group-btn">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Item Attributes</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    @foreach($units as $unit)
                                    @if(!$unit->deleted_at)
                                    <tr>
                                        <td>{{$unit->id}}</td>
                                        <td>{{$unit->item_attribute}}</td>
                                        {{--
                                        <td>
                                            <a href="/uom/{{$unit->id }}"><i class="fa fa-btn fa-search"
                                                                             aria-hidden="true"></i>View</a>
                                        </td>
                                        --}}
                                        <td>
                                            <a href="/uom/{{$unit->id}}/edit"><i class="fa fa-btn fa-pencil-square-o"
                                                                                 aria-hidden="true"></i>Edit</a>
                                        </td>
                                        <td>
                                            <form action="/uom/{{$unit->id}}" method="post" class="pull-left">
                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}
                                                <button class="btn btn-danger btn-sm">
                                                    <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>
{{--Item create modal Starts--}}
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Itrem Attribute</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="/uom" method="post">
                    {{csrf_field()}}

                    <section class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-lg-9">
                                                    <!--<div class="form-group">
                                                        <label for="exampleInputEmail1">Item Name</label>
                                                        <input type="text" name="item_name" class="form-control"
                                                               placeholder="Item Name">
                                                    </div>-->
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Item Attribute/Unit</label>
                                                        <input type="text" name="item_attribute" class="form-control"
                                                               placeholder="Item Attribute/Unit">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="box-footer">
                                    <button type="reset" class="btn btn-default pull-left">Reset</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-success">&nbsp;Save&nbsp;</button>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
