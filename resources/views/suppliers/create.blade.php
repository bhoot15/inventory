@extends('layouts.app')

@section('content')
<div class="content">

    <form role="form" action="/supplier" method="post">
        {{csrf_field()}}
        <section class="content-header">
            <h1>
                Suppliers
                <small>Create</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="/supplier">Suppliers</a></li>
                <li class="active">Create</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Supplier Info</h3>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Supplier Name</label>
                                            <input type="text" name="supplier_name" class="form-control"
                                                   placeholder="supplier Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Company Name</label>
                                            <input type="text" name="company_name" class="form-control"
                                                   placeholder="Company Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Contact Email</label>
                                            <input type="text" name="contact_email" class="form-control"
                                                   placeholder="Contact Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Contact Number</label>
                                            <input type="text" name="contact_phone" class="form-control"
                                                   placeholder="Contact Number">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Website</label>
                                            <input type="text" name="supplier_website" class="form-control"
                                                   placeholder="Website">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3 class="box-title">Payment info</h3><br><br>

                                                <div class="form-group">
                                                    <label>Currency</label>
                                                    <select class="form-control" name="currency">
                                                        <option disabled selected>Select Currency</option>
                                                        <option value="BDT (Taka)">BDT (Taka)</option>
                                                        <option >+ Add New</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3 class="box-title">Billing Address</h3><br><br>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">House</label>
                                                    <input type="text" name="billing_house" class="form-control"
                                                           placeholder="House">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Street</label>
                                                    <input type="text" name="billing_street" class="form-control"
                                                           placeholder="Street">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">City</label>
                                                    <input type="text" name="billing_city" class="form-control"
                                                           placeholder="City">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Zip Code</label>
                                                    <input type="text" name="billing_zip" class="form-control"
                                                           placeholder="Zip Code">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Phone Number</label>
                                                    <input type="text" name="billing_phone" class="form-control"
                                                           placeholder="Phone Number">
                                                </div>
                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <select class="form-control" name="billing_country">
                                                        <option disabled selected>Select Country</option>
                                                        <option>Bangladesh</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3 class="box-title">Shipping Address</h3><br><br>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">House</label>
                                                    <input type="text" name="shipping_house" class="form-control"
                                                           placeholder="House">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Street</label>
                                                    <input type="text" name="shipping_street" class="form-control"
                                                           placeholder="Street">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">City</label>
                                                    <input type="text" name="shipping_city" class="form-control"
                                                           placeholder="City">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Zip Code</label>
                                                    <input type="text" name="shipping_zip" class="form-control"
                                                           placeholder="Zip Code">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Phone Number</label>
                                                    <input type="text" name="shipping_phone" class="form-control"
                                                           placeholder="Phone Number">
                                                </div>
                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <select class="form-control" name="shipping_country">
                                                        <option disabled selected>Select Country</option>
                                                        <option value="Bangladesh">Bangladesh</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="box-footer">
                                                <button type="reset" class="btn btn-default">Cancel</button>
                                                <button type="submit" class="btn btn-info">&nbsp;Save&nbsp;</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </form>
</div>

@endsection
