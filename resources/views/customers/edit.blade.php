@extends('layouts.app')

@section('content')
<div class="content">

    <form role="form" action="/customer/{{$customer->id}}" method="post">
        {{method_field('PUT')}}
        {{csrf_field()}}
        <section class="content-header">
            <h1>
                Customers
                <small>Create</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="/customer">Customers</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Customer Info</h3>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Customer Name</label>
                                            <input type="text" name="customer_name" class="form-control"
                                                   placeholder="Customer Name" value="{{$customer->customer_name}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Company Name</label>
                                            <input type="text" name="company_name" value="{{$customer->company_name}}" class="form-control"
                                                   placeholder="Company Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Contact Email</label>
                                            <input type="text" name="contact_email" value="{{$customer->contact_email}}" class="form-control"
                                                   placeholder="Contact Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Contact Number</label>
                                            <input type="text" name="contact_phone" value="{{$customer->contact_phone}}" class="form-control"
                                                   placeholder="Contact Number">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Website</label>
                                            <input type="text" name="customer_website" value="{{$customer->customer_website}}" class="form-control"
                                                   placeholder="Website">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3 class="box-title">Payment info</h3><br><br>

                                                <div class="form-group">
                                                    <label>Currency</label>
                                                    <select class="form-control" name="currency">
                                                        <option  selected>BDT (Taka)</option>
                                                        <option >+ Add New</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Payment Terms</label>
                                                    <select class="form-control" name="payment_terms">
                                                        <option value="Net 15">Net 15</option>
                                                        <option value="Net 30">Net 30</option>
                                                        <option value="Net 45">Net 45</option>
                                                        <option value="Net 60">Net 60</option>
                                                        <option value="Due End of the Month">Due End of the Month</option>
                                                        <option value="Due End of Next Month">Due End of Next Month</option>
                                                        <option value="Due On Receipt">Due On Receipt</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3 class="box-title">Billing Address</h3><br><br>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">House</label>
                                                    <input type="text" name="billing_house" id="billing_house"
                                                           value="{{$customer->billing_house}}" class="form-control"
                                                           placeholder="House">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Street</label>
                                                    <input type="text" name="billing_street" id="billing_street"
                                                           value="{{$customer->billing_street}}" class="form-control"
                                                           placeholder="Street">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">City</label>
                                                    <input type="text" name="billing_city" id="billing_city"
                                                           value="{{$customer->billing_city}}" class="form-control"
                                                           placeholder="City">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Zip Code</label>
                                                    <input type="text" name="billing_zip" id="billing_zip"
                                                           value="{{$customer->billing_zip}}" class="form-control"
                                                           placeholder="Zip Code">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Phone Number</label>
                                                    <input type="text" name="billing_phone" id="billing_phone"
                                                           value="{{$customer->billing_phone}}" class="form-control"
                                                           placeholder="Phone Number">
                                                </div>
                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <select class="form-control" name="billing_country"
                                                            id="billing_country">
                                                        <option value="Bangladesh" selected >Bangladesh</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3 class="box-title">Shipping Address</h3><a
                                                        class="pull-right" onclick="myFunction()"
                                                        style="cursor: pointer;"><i
                                                            class="fa fa-arrow-down"
                                                            aria-hidden="true"></i> Copy billing address</a><br><br>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">House</label>
                                                    <input type="text" name="shipping_house" id="shipping_house"
                                                           value="{{$customer->shipping_house}}" class="form-control"
                                                           placeholder="House">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Street</label>
                                                    <input type="text" name="shipping_street" id="shipping_street"
                                                           value="{{$customer->shipping_street}}" class="form-control"
                                                           placeholder="Street">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">City</label>
                                                    <input type="text" name="shipping_city" id="shipping_city"
                                                           value="{{$customer->shipping_city}}" class="form-control"
                                                           placeholder="City">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Zip Code</label>
                                                    <input type="text" name="shipping_zip" id="shipping_zip"
                                                           value="{{$customer->shipping_zip}}" class="form-control"
                                                           placeholder="Zip Code">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Phone Number</label>
                                                    <input type="text" name="shipping_phone" id="shipping_phone"
                                                           value="{{$customer->shipping_phone}}" class="form-control"
                                                           placeholder="Phone Number">
                                                </div>
                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <select class="form-control" name="shipping_country"
                                                            id="shipping_country">
                                                        <option value="Bangladesh" selected >Bangladesh</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="box-footer">
                                                <button type="reset" class="btn btn-default">Cancel</button>
                                                <button type="submit" class="btn btn-info">&nbsp;Save&nbsp;</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </form>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    function myFunction() {

        shipping_house.value = document.getElementById("billing_house").value;
        shipping_street.value = document.getElementById("billing_street").value;
        shipping_city.value = document.getElementById("billing_city").value;
        shipping_zip.value = document.getElementById("billing_zip").value;
        shipping_phone.value = document.getElementById("billing_phone").value;
        shipping_country.value = document.getElementById("billing_country").value;


        /* else {
         document.getElementById("shipping_house").value = null;
         shipping_street.value = null;
         shipping_city.value = null;
         shipping_zip.value = null;
         shipping_phone.value = null;
         shipping_country.value = null;
         }*/

    }
</script>
@endpush
