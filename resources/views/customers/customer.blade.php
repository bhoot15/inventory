@extends('layouts.app')

@section('content')
    <div class="content">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Customer Lists
                <small>customers</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Customers</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <!--<h3 class="box-title">customer customers</h3>-->
                            <div class="row">
                                <div class="box-body">
                                    <button type="button" class="btn btn-info" data-toggle="modal"
                                            data-target="#myModal"><i class="fa fa-btn fa-plus" aria-hidden="true"></i>
                                        NEW
                                    </button>
                                    {{--<a href="/customer/create" class="btn btn-info" role="button">
                                        <i class="fa fa-btn fa-plus" aria-hidden="true"></i></a>--}}
                                    {{--<a href="#" class="btn btn-danger" role="button"><i class="fa fa-btn fa-trash"
                                                                                        aria-hidden="true"></i></a>--}}
                                    <br><br>

                                    <div class="box-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control pull-right"
                                                   placeholder="Search">

                                            <div class="input-group-btn">
                                                <button type="submit" class="btn btn-default"><i
                                                            class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="box-body">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Name</th>
                                            <th>Company Name</th>
                                            <th>Email</th>
                                            <th>Contact Number</th>
                                            <th>Receivable</th>
                                            <th>Payable</th>
                                            <th colspan="3">Action</th>
                                        </tr>
                                        @foreach($customers as $customer)
                                            @if(!$customer->deleted_at)
                                                <tr>
                                                    <td>{{$customer->id}}</td>
                                                    <td>
                                                        <a href="/customer/{{$customer->id}}">{{$customer->customer_name}}</a>
                                                    </td>
                                                    <td>{{$customer->company_name}}</td>
                                                    <td>{{$customer->contact_email}}</td>
                                                    <td>{{$customer->contact_phone}}</td>
                                                    <td>0.00</td>
                                                    <td>0.00</td>
                                                    {{--<td>
                                                        <a href="/customer/{{$customer->id }}"><i class="fa fa-btn fa-search" aria-hidden="true"></i>View</a>
                                                    </td>--}}
                                                    {{--<td>
                                                        --}}{{--<a href="/supplier/{{$supplier->id}}/edit"><i class="fa fa-btn fa-pencil-square-o" data-toggle="modal" data-target="#myModalEdit" aria-hidden="true"></i>Edit</a>--}}{{--
                                                        <form action="customer/{{$customer->id}}/edit"
                                                              class="pull-left">
                                                            {{csrf_field()}}
                                                            {{method_field('PUT')}}
                                                            <button type="button" class="btn btn-info btn-sm"
                                                                    data-toggle="modal" data-target="#myModalEdit"><i
                                                                        class="fa fa-btn fa-pencil-square-o"
                                                                        aria-hidden="true"></i>Edit
                                                            </button>
                                                        </form>

                                                    </td>--}}
                                                    <td class="text-left">
                                                        <a href="/customer/{{$customer->id}}"
                                                           class="btn btn-success btn-sm"><i class="fa fa-search"
                                                                                             aria-hidden="true"></i>
                                                            View</a>
                                                    </td>
                                                    <td>
                                                        <a href="/customer/{{$customer->id}}/edit"><i
                                                                    class="fa fa-btn fa-pencil-square-o"
                                                                    aria-hidden="true"></i>Edit</a>
                                                    </td>
                                                    <td>
                                                        <form action="customer/{{$customer->id}}" method="post"
                                                              class="pull-left">
                                                            {{csrf_field()}}
                                                            {{method_field('DELETE')}}
                                                            <button class="btn btn-danger btn-sm">
                                                                <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                                            </button>
                                                        </form>
                                                    </td>

                                                </tr>
                                            @endif
                                        @endforeach


                                    </table>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>

    <!-- Modal CREATE -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create customer</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="/customer" method="post">
                        {{csrf_field()}}
                        {{--<section class="content-header">
                            <h1>
                                customers
                                <small>Create</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                                <li><a href="/customer">Customers</a></li>
                                <li class="active">Create</li>
                            </ol>
                        </section>--}}

                        <section class="content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <h3 class="box-title">Customer Info</h3>

                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-lg-9">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Customer Name</label>
                                                            <input type="text" name="customer_name" class="form-control"
                                                                   placeholder="Customer Name">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Company Name</label>
                                                            <input type="text" name="company_name" class="form-control"
                                                                   placeholder="Company Name">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Contact Email</label>
                                                            <input type="text" name="contact_email" class="form-control"
                                                                   placeholder="Contact Email">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Contact Number</label>
                                                            <input type="text" name="contact_phone" class="form-control"
                                                                   placeholder="Contact Number">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Website</label>
                                                            <input type="text" name="customer_website"
                                                                   class="form-control"
                                                                   placeholder="Website">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="box box-danger">
                                        <div class="box-header">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                                <h3 class="box-title">Payment info</h3><br><br>

                                                                <div class="form-group">
                                                                    <label>Currency</label>
                                                                    <select class="form-control" name="currency">
                                                                        <option value="BDT (Taka)" selected>BDT (Taka)
                                                                        </option>
                                                                        <option>+ Add New</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Payment Terms</label>
                                                                    <select class="form-control" name="payment_terms">
                                                                        <option value="Net 15">Net 15</option>
                                                                        <option value="Net 30">Net 30</option>
                                                                        <option value="Net 45">Net 45</option>
                                                                        <option value="Net 60">Net 60</option>
                                                                        <option value="Due End of the Month">Due End of
                                                                            the Month
                                                                        </option>
                                                                        <option value="Due End of Next Month">Due End of
                                                                            Next Month
                                                                        </option>
                                                                        <option value="Due On Receipt">Due On Receipt
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="box box-success">
                                        <div class="box-header">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                                <h3 class="box-title">Billing
                                                                    Address</h3>{{--<input type="checkbox" id="myCheck" onclick="myFunction()">--}}
                                                                <br><br>

                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">House</label>
                                                                    <input type="text" name="billing_house"
                                                                           id="billing_house"
                                                                           class="form-control"
                                                                           placeholder="House">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Street</label>
                                                                    <input type="text" name="billing_street"
                                                                           id="billing_street"
                                                                           class="form-control"
                                                                           placeholder="Street">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">City</label>
                                                                    <input type="text" name="billing_city"
                                                                           id="billing_city"
                                                                           class="form-control"
                                                                           placeholder="City">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Zip Code</label>
                                                                    <input type="text" name="billing_zip"
                                                                           id="billing_zip"
                                                                           class="form-control"
                                                                           placeholder="Zip Code">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Phone
                                                                        Number</label>
                                                                    <input type="text" name="billing_phone"
                                                                           id="billing_phone"
                                                                           class="form-control"
                                                                           placeholder="Phone Number">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Country</label>
                                                                    <select class="form-control" name="billing_country"
                                                                            id="billing_country">
                                                                        <option value="Bangladesh" selected>Bangladesh
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                                <h3 class="box-title">Shipping Address</h3> <a
                                                                        class="pull-right" onclick="myFunction()"
                                                                        style="cursor: pointer;"><i
                                                                            class="fa fa-arrow-down"
                                                                            aria-hidden="true"></i> Copy billing address</a><br><br>

                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">House</label>
                                                                    <input type="text" name="shipping_house"
                                                                           id="shipping_house"
                                                                           class="form-control"
                                                                           placeholder="House">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Street</label>
                                                                    <input type="text" name="shipping_street"
                                                                           id="shipping_street"
                                                                           class="form-control"
                                                                           placeholder="Street">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">City</label>
                                                                    <input type="text" name="shipping_city"
                                                                           id="shipping_city"
                                                                           class="form-control"
                                                                           placeholder="City">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Zip Code</label>
                                                                    <input type="text" name="shipping_zip"
                                                                           id="shipping_zip"
                                                                           class="form-control"
                                                                           placeholder="Zip Code">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Phone
                                                                        Number</label>
                                                                    <input type="text" name="shipping_phone"
                                                                           id="shipping_phone"
                                                                           class="form-control"
                                                                           placeholder="Phone Number">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Country</label>
                                                                    <select class="form-control"
                                                                            name="shipping_country"
                                                                            id="shipping_country">
                                                                        <option value="Bangladesh" selected>Bangladesh
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{--<div class="row">
                                                            <div class="box-footer">
                                                                <button type="reset" class="btn btn-default">Cancel
                                                                </button>
                                                                <button type="submit" class="btn btn-info">&nbsp;Save&nbsp;</button>
                                                            </div>
                                                        </div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                </div>
                <!-- modal body end div here -->

                <div class="modal-footer">
                    <div class="row">
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-left">Reset</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">&nbsp;Save&nbsp;</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>

        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript">
    function myFunction() {

        shipping_house.value = document.getElementById("billing_house").value;
        shipping_street.value = document.getElementById("billing_street").value;
        shipping_city.value = document.getElementById("billing_city").value;
        shipping_zip.value = document.getElementById("billing_zip").value;
        shipping_phone.value = document.getElementById("billing_phone").value;
        shipping_country.value = document.getElementById("billing_country").value;


        /* else {
         document.getElementById("shipping_house").value = null;
         shipping_street.value = null;
         shipping_city.value = null;
         shipping_zip.value = null;
         shipping_phone.value = null;
         shipping_country.value = null;
         }*/

    }
</script>
@endpush