@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$customer->customer_name}}</h3>
                        <a href="/customer/{{$customer->id}}/edit">
                            <small class="pull-right" style="cursor:pointer;"><i class="fa fa-pencil-square-o"
                                                                                 aria-hidden="true"></i> Edit
                            </small>
                        </a>

                        <p>
                        <hr> {{$customer->contact_email}}<br>
                        {{$customer->contact_phone}}<br>
                        {{$customer->customer_website}}

                        </p>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <strong><i class="fa fa-book margin-r-5"></i> Company Name</strong>

                        <p class="text-muted">
                            {{$customer->company_name}}
                        </p>

                        <hr>

                        <strong><i class="fa fa-map-marker margin-r-5"></i>Billing Address & Details</strong>

                        <p class="text-muted">
                            {{$customer->billing_house}}, {{$customer->billing_street}}, {{$customer->billing_city}}
                            , {{$customer->billing_zip}}, {{$customer->billing_country}}.<br>
                            Billing Phone: {{$customer->billing_phone}}
                        </p>

                        <hr>

                        <strong><i class="fa fa-map-marker margin-r-5"></i>Shipping Address & Details</strong>

                        <p class="text-muted">
                            {{$customer->shipping_house}}, {{$customer->shipping_street}}, {{$customer->shipping_city}}
                            , {{$customer->shipping_zip}}, {{$customer->shipping_country}}.<br>
                            Shipping Phone: {{$customer->shipping_phone}}
                        </p>

                        {{--<hr>

                        <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

                        <p>
                            <span class="label label-danger">UI Design</span>
                            <span class="label label-success">Coding</span>
                            <span class="label label-info">Javascript</span>
                            <span class="label label-warning">PHP</span>
                            <span class="label label-primary">Node.js</span>
                        </p>--}}

                        <hr>

                        {{--<strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                        <p>{{$customer->note}}</p>--}}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#activity" data-toggle="tab">Overview</a></li>
                        <li><a href="#timeline" data-toggle="tab">Transactions</a></li>
                        <li><a href="#settings" data-toggle="tab">Comments</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="activity">
                            <!-- Post -->
                            <div class="post">

                                <div class="row">
                                    <div class="col-md-4">
                                        <!-- Widget: user widget style 1 -->
                                        <div class="box box-widget widget-user-2">
                                            <!-- Add the bg color to the header using any of the bg-* classes -->
                                            <div class="widget-user-header bg-yellow">
                                                <div class="widget-user-image">
                                                    <img class="img-circle" src="../dist/img/avatar5.png"
                                                         alt="User Avatar">
                                                </div>
                                                <!-- /.widget-user-image -->
                                                <h3 class="widget-user-username">{{$customer->customer_name}}</h3>
                                                <h5 class="widget-user-desc">{{$customer->company_name}}</h5>
                                            </div>
                                            <div class="box-footer no-padding">
                                                <ul class="nav nav-stacked">
                                                    <li><a href="#">Email <span
                                                                    class="pull-right badge bg-blue">{{$customer->contact_email}}</span></a>
                                                    </li>
                                                    <li><a href="#">Phone <span
                                                                    class="pull-right badge bg-aqua">{{$customer->contact_phone}}</span></a>
                                                    </li>
                                                    <li><a href="#">Website <span
                                                                    class="pull-right badge bg-green">{{$customer->customer_website}}</span></a>
                                                    </li>
                                                    <li><a href="#">Shipping Phone <span
                                                                    class="pull-right badge bg-red">{{$customer->shipping_phone}}</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.widget-user -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-lg-4 col-xs-12">
                                        <!-- small box -->
                                        <div class="small-box bg-aqua">
                                            <div class="inner">
                                                <h3>৳ 150</h3>

                                                <p>Receivable</p>
                                            </div>
                                            <div class="icon">
                                                {{--<i class="fa fa-shopping-cart"></i>--}}
                                                <i class="fa fa-money"></i>
                                            </div>
                                            <a href="#" class="small-box-footer">
                                                More info <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="timeline">
                            <table class="table table-striped">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Sales Order</th>
                                    <th>Reference</th>
                                    <th>Date</th>
                                    <th>Expected Shipment</th>
                                    <th style="width: 40px">Status</th>
                                </tr>
                                <tr>
                                    <td>1.</td>
                                    <td>SO-001</td>
                                    <td>
                                        Ref-001
                                    </td>
                                    <td>15-05-2017</td>
                                    <td>15-07-2017</td>
                                    <td><span class="badge bg-red">Not Delivered</span></td>
                                </tr>
                                <tr>
                                    <td>1.</td>
                                    <td>SO-007</td>
                                    <td>
                                        Ref-005
                                    </td>
                                    <td>15-01-2017</td>
                                    <td>15-05-2017</td>
                                    <td><span class="badge bg-green-gradient">Delivered</span></td>
                                </tr>

                            </table>
                        </div>
                        <!-- /.tab-pane -->

                        <div class="tab-pane" id="settings">
                            <form class="form-horizontal">
                                {{--<div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Name</label>

                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="inputName" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Name</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputName" placeholder="Name">
                                    </div>
                                </div>--}}
                                <div class="form-group">
                                    <label for="inputExperience" class="col-sm-2 control-label">Comment</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="comment" id="inputExperience"
                                                  placeholder="Comment"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-10 pull-right" style="color: #538192">(For Internal Use)</div>
                                </div>
                                {{--<div class="form-group">
                                    <label for="inputSkills" class="col-sm-2 control-label">Skills</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> I agree to the <a href="#">terms and
                                                    conditions</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>--}}
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
    </section>
@endsection
