<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Stock Inventory Management</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                Inventory Admin
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <!--<ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}">Home</a></li>
            </ul>-->

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Login</a></li>
                <!--<li><a href="{{ url('/register') }}">Register</a></li>-->

                @else
                <li><a href="{{ url('/') }}"><i class="fa fa-btn fa-newspaper-o" aria-hidden="true" style="color: #00695c;"></i>Dashboard</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <i class="fa fa-btn fa-cubes" aria-hidden="true" style="color: #33b5e5;"></i> Stock Items <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/stock') }}"><i class="fa fa-btn fa-bars" aria-hidden="true"></i>Stock Categories</a></li>
                        <li><a href="{{ url('/uom') }}"><i class="fa fa-btn fa-balance-scale" aria-hidden="true"></i>Unit of Management</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('/supplier') }}"><i class="fa fa-btn fa-truck" aria-hidden="true" style="color: #FF8800;"></i>Suppliers</a></li>
                <li><a href="{{ url('/purchase') }}"><i class="fa fa-btn fa-shopping-bag" aria-hidden="true" style="color: #4a148c;"></i>Purchases</a></li>
                <li><a href="{{ url('/customer') }}"><i class="fa fa-btn fa-users" aria-hidden="true" style="color: #c2185b;"></i>Customers</a></li>
                <li><a href="{{ url('/sales') }}"><i class="fa fa-btn fa-bar-chart" aria-hidden="true" style="color: #e65100;"></i>Sales</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <i class="fa fa-btn fa-trophy" aria-hidden="true" style="color: #1a237e;"></i>Outstandings <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/') }}"><i class="fa fa-btn fa-list-ol" aria-hidden="true"></i>Purchase Outstandings</a></li>
                        <li><a href="{{ url('/') }}"><i class="fa fa-btn fa-usd" aria-hidden="true"></i>Sales Outstandings</a></li>
                        <li><a href="{{ url('/tutor') }}"><i class="fa fa-btn fa-money" aria-hidden="true"></i>Payment Transactions</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out" style="color: #ff4444;"></i>Logout</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>

@yield('content')

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
