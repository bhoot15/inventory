<!DOCTYPE html>
    <!--
    This is a starter template page. Use this page to start your new project from
    scratch. This page gets rid of all links and provides the needed markup only.
    -->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Inventory Systems</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        {{--<link rel="stylesheet" type="text/css" href="{{URL::asset('/css/bootstrap.min.css')}}">--}}
        {{--<link rel="stylesheet" type="text/css" href="{{URL::asset('/css/override.css')}}">--}}
        <link rel="stylesheet" type="text/css" href="{{URL::asset('/css/app.css')}}">
        <link rel="stylesheet" href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css')}}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{URL::asset('dist/css/AdminLTE.min.css')}}">
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->

        <link rel="stylesheet" href="{{URL::asset('dist/css/skins/skin-blue.min.css')}}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file://  -->
        <!--[if lt IE 9]>
        <script src="{{URL::asset('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js')}}"></script>
        <script src="{{URL::asset('https://oss.maxcdn.com/respond/1.4.2/respond.min.js')}}"></script>
        <![endif]-->
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="/" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>I</b>S</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Inventory</b>Systems</span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <!-- Menu toggle button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope-o"></i>
                                <span class="label label-success">4</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 4 messages</li>
                                <li>
                                    <!-- inner menu: contains the messages -->
                                    <ul class="menu">
                                        <li><!-- start message -->
                                            <a href="#">
                                                <div class="pull-left">
                                                    <!-- User Image -->
                                                    <img src="{{URL::asset('dist/img/flat.png')}}" class="img-circle" alt="InventorySys">
                                                </div>
                                                <!-- Message title and timestamp -->
                                                <h4>
                                                    Support Team
                                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <!-- The message -->
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <!-- end message -->
                                    </ul>
                                    <!-- /.menu -->
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        <!-- /.messages-menu -->

                        <!-- Notifications Menu -->
                        <li class="dropdown notifications-menu">
                            <!-- Menu toggle button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 10 notifications</li>
                                <li>
                                    <!-- Inner Menu: contains the notifications -->
                                    <ul class="menu">
                                        <li><!-- start notification -->
                                            <a href="#">
                                                <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                            </a>
                                        </li>
                                        <!-- end notification -->
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">View all</a></li>
                            </ul>
                        </li>
                        <!-- Tasks Menu -->
                        <li class="dropdown tasks-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flag-o"></i>
                                <span class="label label-danger">9</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 9 tasks</li>
                                <li>
                                    <!-- Inner menu: contains the tasks -->
                                    <ul class="menu">
                                        <li><!-- Task item -->
                                            <a href="#">
                                                <!-- Task title and progress text -->
                                                <h3>
                                                    Design some buttons
                                                    <small class="pull-right">20%</small>
                                                </h3>
                                                <!-- The progress bar -->
                                                <div class="progress xs">
                                                    <!-- Change the css width attribute to simulate progress -->
                                                    <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">20% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <!-- end task item -->
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#">View all tasks</a>
                                </li>
                            </ul>
                        </li>
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="{{URL::asset('dist/img/flat.png')}}" class="user-image" alt="User Image">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{{Auth::user()->name}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="{{URL::asset('dist/img/flat.png')}}" class="img-circle" alt="User Image">

                                    <p>
                                        {{Auth::user()->name}}
                                        <small>Member Created {{Auth::user()->created_at->diffForHumans()}}</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="row">
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Followers</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Sales</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Friends</a>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <!--<li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>-->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar user panel (optional) -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="{{URL::asset('dist/img/flat.png')}}" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p style="color: #00c0ef"><br>{{Auth::user()->name}}</p>
                        <!-- Status -->
                    </div>
                </div>

                <!-- search form (Optional) -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search...">
                  <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                  </span>
                    </div>
                </form>
                <!-- /.search form -->

                <!-- Sidebar Menu -->
                <ul class="sidebar-menu">
                    <!-- Optionally, you can add icons to the links -->
                    <li class=""><a href="/"><i class="fa fa-btn fa-newspaper-o" aria-hidden="true"></i><span>Dashboard</span></a></li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-btn fa-cubes" aria-hidden="true" style="color: #33b5e5;"></i><span>Items</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="/item"><i class="fa fa-btn fa-bars" aria-hidden="true"></i>Item Lists</a></li>
                            <li><a href="/uom"><i class="fa fa-btn fa-balance-scale" aria-hidden="true"></i>Item Unit</a>
                            </li>
                        </ul>
                    </li>
                    <li class=""><a href="/customer"><i class="fa fa-btn fa-users" aria-hidden="true" style="color: #c2185b;"></i> <span>Customers</span></a></li>
                    <li class=""><a href="/supplier"><i class="fa fa-btn fa-truck" aria-hidden="true" style="color: #FF8800;"></i> <span>Suppliers</span></a></li>
                    <li class=""><a href="/sales"><i class="fa fa-btn fa-bar-chart" aria-hidden="true" style="color: #e65100;"></i> <span>Sales</span></a></li>
                    <li class=""><a href="/purchase"><i class="fa fa-btn fa-shopping-bag" aria-hidden="true" style="color: #ff4444"></i> <span>Purchases</span></a></li>
                    <li class=""><a href="/bills"><i class="fa fa-btn fa-money" aria-hidden="true" style="color: #00C851"></i> <span>Bills</span></a></li>
                    <li class=""><a href="/invoices"><i class="fa fa-btn fa-files-o" aria-hidden="true" style="color: #0088ff;"></i> <span>Invoices</span></a></li>

                    <li class="treeview">
                        <a href="#"><i class="fa fa-btn fa-trophy" aria-hidden="true" style="color: #fafafa"></i> <span>Reports</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="/purchase-outstanding"><i class="fa fa-btn fa-list-ol" aria-hidden="true"></i> Purchases Reports</a></li>
                            <li><a href="/sales-outstanding"><i class="fa fa-btn fa-usd" aria-hidden="true"></i> Sales Reports</a></li>
                            <li><a href="/payment-transaction"><i class="fa fa-btn fa-money" aria-hidden="true"></i> Payment Transactions</a></li>
                        </ul>
                    </li>

                </ul>
                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            @yield('content')

        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="pull-right hidden-xs">

            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2016 - <?php echo date('Y');?> <a href="http://troubleshoot-tech.com">Troubleshoot Technologies</a>.</strong> All rights reserved.
        </footer>

    <script src="{{URL::asset('/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
    <script src="{{URL::asset('/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('/plugins/fastclick/fastclick.js')}}"></script>
    <script src="{{URL::asset('/dist/js/app.js')}}"></script>
    <script src="{{URL::asset('/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{URL::asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{URL::asset('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{URL::asset('/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{URL::asset('/plugins/chartjs/Chart.min.js')}}"></script>
    <script src="{{URL::asset('/dist/js/demo.js')}}"></script>
    <script src="{{URL::asset('/dist/js/pages/dashboard2.js')}}"></script>
    {{--<script src="{{URL::asset('/js/app.js')}}"></script>
    <script src="{{URL::asset('/js/vue.min.js')}}"></script>
    <script src="{{URL::asset('/js/vue-resource.min.js')}}"></script>--}}

    </div>
    </body>
    @stack('scripts')
</html>
