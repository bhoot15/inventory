@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Bills
            <small>list</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Bills</li>
        </ol>
    </section>

    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="clearfix">
                    <a href="/bills/create" class="btn btn-info pull-left"><i class="fa fa-plus"
                                                                              aria-hidden="true"></i> New</a>
                </div>
            </div>
            <div class="panel-body">
                @if($bills->count())
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Bill No.</th>
                            <th>Grand Total</th>
                            <th>Client</th>
                            <th>bill Date</th>
                            <th>Due Date</th>
                            <th colspan="2">Created At</th>
                            <th colspan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bills as $bill)
                            <tr>
                                <td>{{$bill->bill_no}}</td>
                                <td>‎৳ {{$bill->grand_total}}</td>
                                <td>{{$bill->client}}</td>
                                <td>{{$bill->bill_date}}</td>
                                <td>{{$bill->due_date}}</td>
                                <td>{{$bill->created_at->diffForHumans()}}</td>
                                <td class="text-left">
                                    <a href="/bills/{{$bill->id}}"
                                       class="btn btn-success btn-sm"><i class="fa fa-search" aria-hidden="true"></i>
                                        View</a>
                                </td>
                                <td>
                                    <a href="/bills/{{$bill->id}}/edit"
                                       class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                         aria-hidden="true"></i> Edit</a>
                                </td>
                                <td>
                                    <form class="form-inline" onsubmit="return confirm('Are you sure?')"
                                          action="/bills/{{$bill->id}}" method="post" class="pull-left">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button class="btn btn-danger btn-sm">
                                            <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $bills->render() !!}
                @else
                    <div class="invoice-empty">
                        <p class="invoice-empty-title">
                            No bills were created.
                            <a href="/bills/create">Create Now!</a>
                        </p>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection