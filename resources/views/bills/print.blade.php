<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Purchase Bill</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<section class="content">
    <body onload="window.print();">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-2">
                <br><img class="img-responsive" src="/dist/img/logo-black.png">
            </div>
            <div class="col-xs-8 text-center">
                <h3 style="color: #00c0ef;">
                    @foreach ($supplier as $object)
                        {{--{{ $object->company_name }}--}}
                        {{ ucwords(trans($object->company_name)) }}
                </h3>
                <h4>House# {{ $object->billing_house }}, Street# {{ $object->billing_street }}</h4>
                <h4>City# {{ $object->billing_city }} , Zip# {{ $object->billing_zip }}
                    , {{$object->billing_country}}</h4>
                <h4>Phone: {{$object->billing_phone}} ,
                    website:
                    <a target="_blank">{{ $object->supplier_website }}</a>
                    @endforeach
                </h4>
            </div>
            <!-- /.col -->
        </div>
        <br><br>

        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                Bill To
                <address>
                    <strong>{{$purchase->client}}</strong>
                    <br>
                    @foreach ($customer as $co)
                        House# {{ $co->billing_house }} , Street# {{ $co->billing_street }}

                        <br>
                        City# {{ $co->billing_city }} , Zip# {{ $co->billing_zip }}
                        , {{$co->billing_country}}
                        <br>
                        Phone: {{$co->billing_phone}}<br>
                        Email: {{$co->contact_email}}
                    @endforeach
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                Shift To
                <address>
                    <strong>{{$purchase->client}}</strong>
                    <br>
                    @foreach ($customer as $co)
                        House# {{ $co->shipping_house }} , Street# {{ $co->shipping_street }}

                        <br>
                        City# {{ $co->shipping_city }} , Zip# {{ $co->shipping_zip }}
                        , {{$co->shipping_country}}
                        <br>
                        Phone: {{$co->shipping_phone}}<br>
                    @endforeach
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>#Purchase - {{$purchase->purchase_no}}</b><br>
                <br>
                <b>Order ID:</b> {{$purchase->purchase_order_number}}<br>
                <b>Payment Due:</b> {{$purchase->due_date}}<br>
                <b>Purchase Reference:</b> {{$purchase->purchase_reference}}
            </div>
            <!-- /.col -->
        </div>

        <hr>

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Product Name</th>
                <th>Price</th>
                <th>Qty</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($purchase->products as $product)
                <tr>
                    <td class="table-name">{{$product->name}}</td>
                    <td class="table-price">${{$product->price}}</td>
                    <td class="table-qty">{{$product->qty}}</td>
                    <td class="table-total"> ৳ {{$product->qty * $product->price}}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td class="table-empty" colspan="2"></td>
                <td class="table-label">Sub Total</td>
                <td class="table-amount"> ৳ {{$purchase->sub_total}}</td>
            </tr>
            <tr>
                <td class="table-empty" colspan="2"></td>
                <td class="table-label">Discount</td>
                <td class="table-amount"> ৳ {{$purchase->discount}}</td>
            </tr>
            <tr>
                <td class="table-empty" colspan="2"></td>
                <td class="table-label">Grand Total</td>
                <td class="table-amount"> ৳ {{$purchase->grand_total}}</td>
            </tr>
            </tfoot>
        </table>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-7 form-group pull-left">
                    <br>

                    <p>
                        Thank you for your business!
                        <br>
                        Should you have any enquiries concerning this invoice, please contact
                        <br>
                        <b>{{$purchase->supplier_name}}</b>
                        <br>
                        @foreach($supplier as $object)
                            Voice: {{$object->contact_phone}}
                            <br>
                            Email: {{$object->contact_email}}
                            <br>
                            {{ ucwords(trans($object->company_name)) }}
                            <br>
                            House# {{ $object->billing_house }}, Street# {{ $object->billing_street }}
                            City# {{ $object->billing_city }} , Zip# {{ $object->billing_zip }}
                            , {{$object->billing_country}}
                            <br>
                            Visit Us:
                            <a>{{ $object->supplier_website }}</a>
                        @endforeach
                    </p>
                </div>

                <div class="col-md-5 form-group pull-right">
                    <br><label>___________________________</label> <br>

                    <small class="text-center">Authorized Signature & Date</small>

                </div>
            </div>
        </div>
    </div>
    </body>
</section>

</html>