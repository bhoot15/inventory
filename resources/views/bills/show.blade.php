@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="clearfix">
                    <span class="panel-title">Bill</span>

                    <div class="pull-right">
                        <table class="table-responsive">
                            <tr>
                                <td><a href="/bills" class="btn btn-info"><i class="fa fa-arrow-left"
                                                                             aria-hidden="true"></i> Back </a></td>
                                <td><a href="/bills/{{$bill->id}}/edit" class="btn btn-primary">Edit</a></td>
                                <td>
                                    <form class="form-inline" onsubmit="return confirm('Are you sure?')"
                                          action="/bills/{{$bill->id}}" method="post" class="pull-left">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button class="btn btn-danger">
                                            <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                                zz
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>bill No.</label>

                            <p>{{$bill->bill_no}}</p>
                        </div>
                        <div class="form-group">
                            <label>Grand Total</label>

                            <p> ৳ {{$bill->grand_total}}</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Client</label>

                            <p>{{$bill->client}}</p>
                        </div>
                        <div class="form-group">
                            <label>Client Address</label>
                            <pre class="pre">{{$bill->client_address}}</pre>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Title</label>

                            <p>{{$bill->title}}</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>bill Date</label>

                                <p>{{$bill->bill_date}}</p>
                            </div>
                            <div class="col-sm-6">
                                <label>Due Date</label>

                                <p>{{$bill->due_date}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bill->products as $product)
                        <tr>
                            <td class="table-name">{{$product->name}}</td>
                            <td class="table-price">${{$product->price}}</td>
                            <td class="table-qty">{{$product->qty}}</td>
                            <td class="table-total"> ৳ {{$product->qty * $product->price}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Sub Total</td>
                        <td class="table-amount"> ৳ {{$bill->sub_total}}</td>
                    </tr>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Discount</td>
                        <td class="table-amount"> ৳ {{$bill->discount}}</td>
                    </tr>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Grand Total</td>
                        <td class="table-amount"> ৳ {{$bill->grand_total}}</td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
@endsection