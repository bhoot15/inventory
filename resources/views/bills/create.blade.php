@extends('layouts.app')

@section('content')
    <section class="content">
        <div id="bills">
            <div class="panel panel-default" v-cloak>
                <div class="panel-heading">
                    <div class="clearfix">
                        <span class="panel-title">Create Bill</span>
                        <a href="/bills" class="btn btn-default pull-right">Back</a>
                    </div>
                </div>
                <div class="panel-body">
                    @include('bills.form')
                </div>
                <div class="panel-footer">
                    <a href="/bills" class="btn btn-default">CANCEL</a>
                    <button class="btn btn-success" @click="create" :disabled="isProcessing">CREATE</button>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="/js/vue.min.js"></script>
<script src="/js/vue-resource.min.js"></script>
<script type="text/javascript">
    Vue.http.headers.common['X-CSRF-TOKEN'] = '{{csrf_token()}}';

    window._form = {
        bill_no: '',
        client: '',
        client_address: '',
        title: '',
        bill_date: '',
        due_date: '',
        discount: 0,
        products: [{
            name: '',
            price: 0,
            qty: 1
        }]
    };
</script>
<script src="/js/bill.js"></script>
@endpush