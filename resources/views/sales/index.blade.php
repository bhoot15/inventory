@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Sales
            <small>list</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Sales</li>
        </ol>
    </section>

    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="clearfix">
                    <a href="/sales/create" class="btn btn-info pull-left"><i class="fa fa-plus"
                                                                              aria-hidden="true"></i> New</a>
                </div>
            </div>
            <div class="panel-body">
                @if($sales->count())
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Sales No.</th>
                            <th>Grand Total</th>
                            <th>Customer Name</th>
                            <th>Sales Date</th>
                            <th>Due Date</th>
                            <th colspan="2">Created At</th>
                            <th colspan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sales as $sale)
                            <tr>
                                <td>{{$sale->sales_no}}</td>
                                <td>‎৳ {{$sale->grand_total}}</td>
                                <td>{{$sale->sales_customer_name}}</td>
                                <td>{{$sale->sales_date}}</td>
                                <td>{{$sale->due_date}}</td>
                                <td>{{$sale->created_at->diffForHumans()}}</td>
                                <td class="text-left">
                                    <a href="/sales/{{$sale->id}}"
                                       class="btn btn-success btn-sm"><i class="fa fa-search" aria-hidden="true"></i>
                                        View</a>
                                </td>
                                <td>
                                    <a href="/sales/{{$sale->id}}/edit"
                                       class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                         aria-hidden="true"></i> Edit</a>
                                </td>
                                <td>
                                    <form class="form-inline" onsubmit="return confirm('Are you sure?')"
                                          action="/sales/{{$sale->id}}" method="post" class="pull-left">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button class="btn btn-danger btn-sm">
                                            <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                    {{--<form class="form-inline" method="post"
                                          action="{{route('sales.destroy', $sale)}}"
                                          onsubmit="return confirm('Are you sure?')"
                                            >
                                        <input type="hidden" name="_method" value="delete">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button class="btn btn-danger btn-sm">
                                            <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $sales->render() !!}
                @else
                    <div class="invoice-empty">
                        <p class="invoice-empty-title">
                            No sales were created.
                            <a href="/sales/create">Create Now!</a>
                        </p>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection