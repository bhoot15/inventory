@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="clearfix">
                    <span class="panel-title">Sale</span>

                    <div class="pull-right">
                        {{--<a href="{{route('sales.index')}}" class="btn btn-default">Back</a>--}}
                        <table class="table-responsive">
                            <tr>
                                <td><a href="/sales" class="btn btn-info"><i class="fa fa-arrow-left"
                                                                             aria-hidden="true"></i> Back </a></td>
                                <td><a href="/sales/{{$sale->id}}/edit" class="btn btn-primary">Edit</a></td>
                                <td>
                                    <form class="form-inline" onsubmit="return confirm('Are you sure?')"
                                          action="/sales/{{$sale->id}}" method="post" class="pull-left">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button class="btn btn-danger">
                                            <i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Customer Name</label>

                            <p>{{$sale->sales_customer_name}}</p>
                        </div>
                        <div class="form-group">
                            <label>Sale Order</label>

                            <p>{{$sale->sales_order}}</p>
                        </div>
                        <div class="form-group">
                            <label>Sale No.</label>

                            <p>{{$sale->sales_no}}</p>
                        </div>
                        <div class="form-group">
                            <label>Grand Total</label>

                            <p> ৳ {{$sale->grand_total}}</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sales Reference</label>

                            <p>{{$sale->sales_reference}}</p>
                        </div>
                        <div class="form-group">
                            <label>Client</label>

                            <p>{{$sale->client}}</p>
                        </div>
                        <div class="form-group">
                            <label>Sales Person</label>

                            <p>{{$sale->sales_person}}</p>
                        </div>
                        <div class="form-group">
                            <label>Client Address</label>
                            <pre class="pre">{{$sale->client_address}}</pre>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Title</label>

                            <p>{{$sale->title}}</p>
                        </div>
                        <div class="form-group">
                            <label>Delivery Method</label>

                            <p>{{$sale->sales_delivery_method}}</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Sales Date</label>

                                <p>{{$sale->sales_date}}</p>
                            </div>
                            <div class="col-sm-6">
                                <label>Due Date</label>

                                <p>{{$sale->due_date}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sale->products as $product)
                        <tr>
                            <td class="table-name">{{$product->name}}</td>
                            <td class="table-price">${{$product->price}}</td>
                            <td class="table-qty">{{$product->qty}}</td>
                            <td class="table-total"> ৳ {{$product->qty * $product->price}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Sub Total</td>
                        <td class="table-amount"> ৳ {{$sale->sub_total}}</td>
                    </tr>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Discount</td>
                        <td class="table-amount"> ৳ {{$sale->discount}}</td>
                    </tr>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Grand Total</td>
                        <td class="table-amount"> ৳ {{$sale->grand_total}}</td>
                    </tr>
                    </tfoot>
                </table>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 pull-right">
                        <div class="form-group">
                            <label>Sales Notes</label>
                            <textarea class="form-control" rows="3" disabled
                                      name="sales_note">{{$sale->sales_note}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 pull-right">
                        <div class="form-group">
                            <label>Terms & Condition</label>
                            <textarea class="form-control" rows="3" disabled
                                    >{{$sale->sales_tnc}}</textarea>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection