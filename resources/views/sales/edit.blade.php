@extends('layouts.app')

@section('content')
    <section class="content">
        <div id="sales">
            <div class="panel panel-default" v-cloak>
                <div class="panel-heading">
                    <div class="clearfix">
                        <span class="panel-title">Edit Sales</span>
                        <a href="/sales" class="btn btn-default pull-right">Back</a>
                    </div>
                </div>
                <div class="panel-body">
                    @include('sales.form')
                </div>
                <div class="panel-footer">
                    <a href="/sales" class="btn btn-default">CANCEL</a>
                    <button class="btn btn-success" @click="update" :disabled="isProcessing">UPDATE</button>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>
<script src="/js/vue.min.js"></script>
<script src="/js/vue-resource.min.js"></script>
<script type="text/javascript">
    Vue.http.headers.common['X-CSRF-TOKEN'] = '{{csrf_token()}}';
    /*window._form = {!!$sale -> toJson()!!}; //this is correct format*/
    window._form = {
    !!$sale - > toJson()
    !!
    }
    ;
</script>
<script src="/js/sales.js"></script>
@endpush