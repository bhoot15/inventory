<style>
    p {
        color: #ff0000;
    }
</style>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Customer Name</label>
            <select class="form-control" id="myselect" name="sales_customer_name" v-model="form.sales_customer_name">
                <option disabled>Select Customer</option>
                @foreach($customers as $customer)
                    <option>{{$customer->customer_name}}</option>
                @endforeach
                {{--<option value="secondoption">+ Add New</option>--}}
            </select>

            <p v-if="errors.sales_customer_name" class="error">@{{errors.sales_customer_name[0]}}</p>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Sales Order</label>
            <input type="text" name="sales_order" class="form-control"
                   placeholder="sales order number" v-model="form.sales_order">

            <p v-if="errors.sales_order" class="error">@{{errors.sales_order[0]}}</p>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Reference</label>
            <input type="text" name="sales_reference" class="form-control"
                   placeholder="sales Reference" v-model="form.sales_reference">

            <p v-if="errors.sales_reference" class="error">@{{errors.sales_reference[0]}}</p>
        </div>
        <div class="form-group">
            <label>Sales Person</label>
            <select class="form-control" name="sales_person" v-model="form.sales_person">
                <option disabled>Select Sales Person</option>
                @foreach($suppliers as $supplier)
                    <option>{{$supplier->supplier_name}}</option>
                @endforeach
            </select>

            <p v-if="errors.sales_person" class="error">@{{errors.sales_person[0]}}</p>
        </div>
        <div class="form-group">
            <label>Delivery Method</label>
            <select class="form-control" name="sales_delivery_method" v-model="form.sales_delivery_method">
                <option disabled selected>Select Delivery Method</option>
                <option>Cash On Delivery</option>
                <option>Card Payment</option>
                <option>Other</option>
            </select>

            <p v-if="errors.sales_delivery_method" class="error">@{{errors.sales_delivery_method[0]}}</p>
        </div>

        <div class="form-group">
            <label>Sales No.</label>
            <input type="text" class="form-control" v-model="form.sales_no">

            <p v-if="errors.sales_no" class="error">@{{errors.sales_no[0]}}</p>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Client Address</label>
            <textarea class="form-control" v-model="form.client_address"></textarea>

            <p v-if="errors.client_address" class="error">@{{errors.client_address[0]}}</p>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" v-model="form.title">

            <p v-if="errors.title" class="error">@{{errors.title[0]}}</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <label>Sales Date</label>
                <input type="date" class="form-control" v-model="form.sales_date">

                <p v-if="errors.sales_date" class="error">@{{errors.sales_date[0]}}</p>
            </div>
            <div class="col-sm-6">
                <label>Due Date</label>
                <input type="date" class="form-control" v-model="form.due_date">

                <p v-if="errors.due_date" class="error">@{{errors.due_date[0]}}</p>
            </div>
        </div>
    </div>
</div>
<hr>
<div v-if="errors.products_empty">
    <p class="alert alert-danger">@{{errors.products_empty[0]}}</p>
    <hr>
</div>
<table class="table table-bordered table-form">
    <thead>
    <tr>
        <th>Product Name</th>
        <th>Price</th>
        <th>Qty</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    <tr v-for="product in form.products">
        <td class="table-name" :class="{'table-error': errors['products.' + $index + '.name']}">
            {{--<textarea class="table-control" v-model="product.name"></textarea>--}}

            <select class="form-control" id="name" v-model="product.name">
                <option disabled>Please Select Item</option>
                @foreach($items as $item)
                    <option @click="selectItem" value="{{$item->item_sales_selling_price}}"
                    >{{$item->item_name}}</option>
                @endforeach
            </select>
        </td>
        <td class="table-price" :class="{'table-error': errors['products.' + $index + '.price']}">
            {{--<input type="text" class="table-control"  v-model="product.price">--}}
            <input type="text" class="form-control" v-model="product.price" id="price">
        </td>
        <td class="table-qty" :class="{'table-error': errors['products.' + $index + '.qty']}">
            <input type="text" class="form-control" v-model="product.qty">
        </td>
        <td class="table-total">
            <span class="table-text" id="total">@{{product.qty * product.price}}</span>
        </td>
        <td class="table-remove">
            <span @click="remove(product)" class="table-remove-btn"><i class="fa fa-trash-o"
                                                                       aria-hidden="true"></i></span>
        </td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td class="table-empty" colspan="2">
            <span @click="addLine" class="table-add_line"><font style="cursor: pointer; color: #00c0ef">Add
                Line</font></span>
        </td>
        <td class="table-label">Sub Total</td>
        <td class="table-amount">@{{subTotal}}</td>
    </tr>
    <tr>
        <td class="table-empty" colspan="2"></td>
        <td class="table-label">Discount</td>
        <td class="table-discount" :class="{'table-error': errors.discount}">
            <input type="text" class="table-discount_input" v-model="form.discount">
        </td>
    </tr>
    <tr>
        <td class="table-empty" colspan="2"></td>
        <td class="table-label">Grand Total</td>
        <td class="table-amount">@{{grandTotal}}</td>
    </tr>
    </tfoot>
</table>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 pull-right">
        <div class="form-group">
            <label>Sales Notes</label>
            <textarea class="form-control" rows="3"
                      placeholder="Notes"
                      name="sales_note" v-model="form.sales_note"></textarea>

            <p v-if="errors.sales_note" class="error">@{{errors.sales_note[0]}}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 pull-right">
        <div class="form-group">
            <label>Terms & Condition</label>
            <textarea class="form-control" rows="3"
                      placeholder="Terms & Condition"
                      name="sales_tnc" v-model="form.sales_tnc"></textarea>

            <p v-if="errors.sales_tnc" class="error">@{{errors.sales_tnc[0]}}</p>
        </div>
    </div>
</div>